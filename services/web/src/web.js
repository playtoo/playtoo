'use module';

import http from 'http';
import spdy from 'spdy';
import createLogger from 'debug';
import express from 'express';
import cookieParser from 'cookie-parser';
import fs from 'fs';
import path from 'path';

import routes from './routes';
import passport from './services/auth';
import { connect as dbConnect } from './data/db';
import config from './config';
import { connect as sessionConnect, middleware as sessionMiddleware } from './services/session';

const { HTTP_WEB_PORT, HTTPS_WEB_PORT, SSL_CERT_PATH } = config;
const log = createLogger('server');

const createApp = () => {
  const app = express();

  app.use(express.static('static'));
  app.use(cookieParser());
  app.use(sessionMiddleware());
  app.use(passport.initialize());
  app.use(passport.session());
  app.use('/', routes());

  return app;
};

const start = async () => {
  await Promise.all([dbConnect(), sessionConnect()]);
  const app = createApp();
  const options = {
    key: fs.readFileSync(path.join(SSL_CERT_PATH, 'privkey.pem')),
    cert: fs.readFileSync(path.join(SSL_CERT_PATH, 'fullchain.pem')),
  };

  // HTTP server
  const httpServer = http.createServer(app);
  httpServer.listen(HTTP_WEB_PORT, () => log(`Listening on ${httpServer.address().port}`));
  httpServer.on('error', err => console.error(err));

  // HTTPS server
  const http2Server = spdy.createServer(options, app);
  http2Server.listen(HTTPS_WEB_PORT, () => log(`Listening on ${http2Server.address().port}`));
  http2Server.on('error', err => console.error(err));
};

start();
