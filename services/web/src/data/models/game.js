module.exports = (sequelize, DataTypes) => {
  const game = sequelize.define('game', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    igdbId: {
      type: DataTypes.STRING,
      unique: true,
    },
    slug: DataTypes.STRING,
    popularity: DataTypes.DOUBLE,
    rating: DataTypes.DOUBLE,
    name: DataTypes.STRING,
    art: DataTypes.STRING,
    featured: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  });

  game.associate = models => {
    game.belongsToMany(models.user, { through: models.usergame });
    models.user.belongsToMany(game, { through: models.usergame });
  };

  return game;
};
