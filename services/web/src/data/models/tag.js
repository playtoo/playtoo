module.exports = (sequelize, DataTypes) => {
  const tag = sequelize.define('tag', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    tag: DataTypes.STRING,
    userId: DataTypes.UUID,
  });

  tag.associate = models => {
    models.tag.belongsTo(models.user);
    models.user.hasMany(models.tag);
  };

  return tag;
};
