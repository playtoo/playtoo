module.exports = (sequelize, DataTypes) => {
  const usergame = sequelize.define('usergame', {
    lastPlayed: DataTypes.DATE,
  });
  return usergame;
};
