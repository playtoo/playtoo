module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    username: DataTypes.STRING,
    avatar: DataTypes.STRING,
    googleId: DataTypes.STRING,
    twitterId: DataTypes.STRING,
    facebookId: DataTypes.STRING,
    discordId: DataTypes.STRING,
    googleAvatar: DataTypes.STRING,
    twitterAvatar: DataTypes.STRING,
    facebookAvatar: DataTypes.STRING,
    discordAvatar: DataTypes.STRING,
    publicProfile: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    emailNotifications: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    publicPlayInvites: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
  });

  return user;
};
