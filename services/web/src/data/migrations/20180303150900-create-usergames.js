module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('usergames', {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
        type: Sequelize.UUID,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      lastPlayed: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      gameId: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      userId: {
        allowNull: false,
        type: Sequelize.UUID,
      },
    });

    await queryInterface.addConstraint('usergames', ['gameId'], {
      type: 'foreign key',
      name: 'usergames_gameId_fkey',
      references: {
        table: 'games',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('usergames', ['userId'], {
      type: 'foreign key',
      name: 'usergames_userId_fkey',
      references: {
        table: 'users',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },

  down: queryInterface => queryInterface.dropTable('usergames'),
};
