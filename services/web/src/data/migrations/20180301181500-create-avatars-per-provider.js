module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('users', 'googleAvatar', {
      type: Sequelize.STRING,
    });

    await queryInterface.addColumn('users', 'twitterAvatar', {
      type: Sequelize.STRING,
    });

    await queryInterface.addColumn('users', 'facebookAvatar', {
      type: Sequelize.STRING,
    });

    await queryInterface.addColumn('users', 'discordAvatar', {
      type: Sequelize.STRING,
    });

    await queryInterface.sequelize.query(`
      UPDATE users
      SET
        "discordAvatar" = avatar,
        "facebookAvatar" = avatar,
        "twitterAvatar" = avatar,
        "googleAvatar" = avatar
    `);

    await queryInterface.sequelize.query(`
      UPDATE users
      SET
        "avatar" = 'google'
    `);
  },

  down: queryInterface =>
    Promise.all([
      queryInterface.removeColumn('users', 'googleAvatar'),
      queryInterface.removeColumn('users', 'twitterAvatar'),
      queryInterface.removeColumn('users', 'facebookAvatar'),
      queryInterface.removeColumn('users', 'discordAvatar'),
    ]),
};
