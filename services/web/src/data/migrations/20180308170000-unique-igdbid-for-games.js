module.exports = {
  up: queryInterface =>
    queryInterface.addConstraint('games', ['igdbId'], {
      type: 'unique',
      name: 'unique_igdbid',
    }),

  down: queryInterface => queryInterface.removeConstraint('games', 'unique_igdbid'),
};
