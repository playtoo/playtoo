module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('users', 'discordId', {
      type: Sequelize.STRING,
    }),
  down: queryInterface => queryInterface.removeColumn('users', 'discordId'),
};
