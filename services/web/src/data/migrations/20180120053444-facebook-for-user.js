module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('users', 'facebookId', {
      type: Sequelize.STRING,
    }),
  down: queryInterface => queryInterface.removeColumn('users', 'facebookId'),
};
