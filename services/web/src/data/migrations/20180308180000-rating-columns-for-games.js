module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('games', 'slug', {
      type: Sequelize.STRING,
    });

    await queryInterface.addColumn('games', 'popularity', {
      type: Sequelize.DOUBLE,
    });

    await queryInterface.addColumn('games', 'rating', {
      type: Sequelize.DOUBLE,
    });

    return Promise.resolve();
  },

  down: async queryInterface => {
    await queryInterface.removeColumn('games', 'slug');
    await queryInterface.removeColumn('games', 'popularity');
    await queryInterface.removeColumn('games', 'rating');
    return Promise.resolve();
  },
};
