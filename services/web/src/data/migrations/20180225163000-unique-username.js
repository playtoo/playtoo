module.exports = {
  up: queryInterface =>
    queryInterface.addConstraint('users', ['username'], {
      type: 'unique',
      name: 'unique_username',
    }),

  down: queryInterface => queryInterface.removeConstraint('users', 'unique_username'),
};
