module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('users', 'twitterId', {
      type: Sequelize.STRING,
    }),
  down: queryInterface => queryInterface.removeColumn('users', 'twitterId'),
};
