module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('users', 'publicProfile', {
      type: Sequelize.BOOLEAN,
    });

    await queryInterface.addColumn('users', 'emailNotifications', {
      type: Sequelize.BOOLEAN,
    });

    await queryInterface.addColumn('users', 'publicPlayInvites', {
      type: Sequelize.BOOLEAN,
    });

    await queryInterface.sequelize.query(`
      UPDATE users
      SET
        "publicProfile" = TRUE,
        "emailNotifications" = TRUE,
        "publicPlayInvites" = TRUE
    `);
  },

  down: queryInterface =>
    Promise.all([
      queryInterface.removeColumn('users', 'publicProfile'),
      queryInterface.removeColumn('users', 'emailNotifications'),
      queryInterface.removeColumn('users', 'publicPlayInvites'),
    ]),
};
