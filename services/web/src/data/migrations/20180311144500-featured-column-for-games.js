module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('games', 'featured', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });

    return queryInterface.sequelize.query(`
      UPDATE games
      SET
        "featured" = false
    `);
  },

  down: async queryInterface => queryInterface.removeColumn('games', 'featured'),
};
