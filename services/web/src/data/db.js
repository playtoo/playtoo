import Sequelize from 'sequelize';
import path from 'path';
import Umzug from 'umzug';
import loadDbModels from './models';
import config from '../config';

const { POSTGRESQL_HOST, DB_NAME, DB_PASS, POSTGRESQL_PORT, DB_USER } = config;
let sequelize;

export const connect = async () => {
  sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
    host: POSTGRESQL_HOST,
    port: POSTGRESQL_PORT,
    dialect: 'postgres',
    logging: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  });

  loadDbModels(sequelize);

  const umzug = new Umzug({
    storage: 'sequelize',
    storageOptions: {
      sequelize,
    },
    logging: console.log,
    migrations: {
      path: path.resolve(__dirname, '../data/migrations'),
      params: [sequelize.getQueryInterface(), sequelize.Sequelize],
    },
  });
  await umzug.up();
  return sequelize;
};

export const isConnected = async () => {
  try {
    await sequelize.authenticate();
    return true;
  } catch (e) {
    return false;
  }
};

export const models = () => sequelize.models;

export const getRandomFn = () => sequelize.random();
