import express from 'express';
import bodyParser from 'body-parser';
// import graphqlHTTP from 'express-graphql';

// import { schema, resolvers } from '../services/relay';
// import resolvers from '../services/relay/resolvers';

import * as graphql from '../services/graphql';

export default () => {
  const app = express();

  app.use(
    '/graphql',
    bodyParser.json(),
    graphql.graphqlHandler(req => ({
      authenticatedUser: req.user,
    })),
  );
  app.use('/graphiql', graphql.graphiqlHandler('/graphql'));

  // app.use(
  //   '/relay',
  //   graphqlHTTP(req => ({
  //     schema,
  //     rootValue: resolvers,
  //     graphiql: true,
  //     context: {
  //       authenticatedUser: req.user,
  //     },
  //   })),
  // );

  return app;
};
