import express from 'express';
import * as db from '../data/db';
import config from '../config';

const { POSTGRESQL_HOST, DB_NAME, POSTGRESQL_PORT, DB_USER, REDIS_HOST, REDIS_PORT } = config;

export default () => {
  const app = express();

  app.get('/', async (req, res) => {
    const [isDbConnected] = await Promise.all([db.isConnected()]);

    res.send(`
    <pre>
        DB connection: ${isDbConnected ? 'ok' : 'FAIL'}
        DB User: ${DB_USER}
        DB Name: ${DB_NAME}
        DB Host: ${POSTGRESQL_HOST}
        DB Post: ${POSTGRESQL_PORT}

        SESSION Host: ${REDIS_HOST}
        SESSION Post: ${REDIS_PORT}
    </pre>
    `);
  });
  return app;
};
