import fs from 'fs';
import path from 'path';
import express from 'express';
import graphql from './graphql';
import healthcheck from './healthcheck';
import auth from './auth';

const index = fs.readFileSync(path.resolve(__dirname, '../../static/index.html'));

export default () => {
  const app = express();

  app.get(
    ['/', '/games', '/players', '/friends', '/messages', '/edit-profile', /\/players\/[^/]+$/],
    (req, res) => {
      res.set('Content-Type', 'text/html');
      res.send(index);
    },
  );

  app.use('/', graphql());
  app.use('/', auth());
  app.use('/healthcheck', healthcheck());

  return app;
};
