'use module';

import { installResolvers, app, fillGames } from './services/jobs';
import { connect as dbConnect, models } from './data/db';
import config from './config';

const { HTTP_JOBS_PORT } = config;

const start = async () => {
  await dbConnect();
  installResolvers(models);

  fillGames();

  app.listen(HTTP_JOBS_PORT);
};

start();
