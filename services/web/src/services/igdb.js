import igdb from 'igdb-api-node';
import { IGDB_API_KEY } from '../config';

const client = igdb.default(IGDB_API_KEY);

export default client;
