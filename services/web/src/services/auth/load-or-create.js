import suggestUsername from 'suggest-username';
import { models } from '../../data/db';

export default async ({ user, profileId, avatarUrl, avatarColumn, idColumn }) => {
  if (user) {
    // eslint-disable-next-line no-param-reassign
    user[idColumn] = profileId;
    // eslint-disable-next-line no-param-reassign
    user[avatarColumn] = avatarUrl;
    await user.save();
    return user;
  }

  const [dbUser, isNew] = await models().user.findCreateFind({
    where: {
      [idColumn]: profileId,
    },
  });

  if (isNew) {
    let numModels;
    let username;
    do {
      username = await suggestUsername(2, '.', 'randomNumbers');
      numModels = await models().user.count({ where: { username } });
    } while (numModels > 0);
    dbUser.username = username;
  }

  dbUser[idColumn] = profileId;
  dbUser[avatarColumn] = avatarUrl;
  await dbUser.save();
  return dbUser;
};
