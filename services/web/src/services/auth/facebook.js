import passport from 'passport';
import PassportFacebook from 'passport-facebook';
import config from '../../config';
import loadOrCreate from './load-or-create';

const { FACEBOOK_OAUTH_CLIENT_ID, FACEBOOK_OAUTH_CLIENT_SECRET, FACEBOOK_OAUTH_URL } = config;

passport.use(
  new PassportFacebook.Strategy(
    {
      clientID: FACEBOOK_OAUTH_CLIENT_ID,
      clientSecret: FACEBOOK_OAUTH_CLIENT_SECRET,
      callbackURL: FACEBOOK_OAUTH_URL,
      passReqToCallback: true,
      profileFields: ['id', 'picture.type(large)'],
    },
    async (req, accessToken, refreshToken, profile, done) => {
      const user = await loadOrCreate({
        user: req.user,
        profileId: profile.id,
        idColumn: 'facebookId',
        avatarColumn: 'facebookAvatar',
        avatarUrl: profile.photos[0].value,
      });
      done(null, user);
    },
  ),
);
