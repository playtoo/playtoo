import passport from 'passport';
import GoogleStrategy from 'passport-google-oauth';
import config from '../../config';
import loadOrCreate from './load-or-create';

const { GOOGLE_OAUTH_CLIENT_ID, GOOGLE_OAUTH_CLIENT_SECRET, GOOGLE_OAUTH_URL } = config;

passport.use(
  new GoogleStrategy.OAuth2Strategy(
    {
      clientID: GOOGLE_OAUTH_CLIENT_ID,
      clientSecret: GOOGLE_OAUTH_CLIENT_SECRET,
      callbackURL: GOOGLE_OAUTH_URL,
      passReqToCallback: true,
    },
    async (req, accessToken, refreshToken, profile, done) => {
      const user = await loadOrCreate({
        user: req.user,
        profileId: profile.id,
        idColumn: 'googleId',
        avatarColumn: 'googleAvatar',
        avatarUrl: profile.photos[0].value.replace(/sz=\d+/, 'sz=128'),
      });
      done(null, user);
    },
  ),
);
