import passport from 'passport';
import PassportDiscord from 'passport-discord';
import config from '../../config';
import loadOrCreate from './load-or-create';

const { DISCORD_OAUTH_CLIENT_ID, DISCORD_OAUTH_CLIENT_SECRET, DISCORD_OAUTH_URL } = config;

passport.use(
  'discord',
  new PassportDiscord.Strategy(
    {
      clientID: DISCORD_OAUTH_CLIENT_ID,
      clientSecret: DISCORD_OAUTH_CLIENT_SECRET,
      callbackURL: DISCORD_OAUTH_URL,
      passReqToCallback: true,
    },
    async (req, accessToken, refreshToken, profile, done) => {
      const user = await loadOrCreate({
        user: req.user,
        profileId: profile.id,
        idColumn: 'discordId',
        avatarColumn: 'discordAvatar',
        avatarUrl: `https://cdn.discordapp.com/avatars/${profile.id}/${
          profile.avatar
        }.png?size=128`,
      });
      done(null, user);
    },
  ),
);
