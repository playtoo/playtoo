import passport from 'passport';
import PassportTwitter from 'passport-twitter';
import config from '../../config';
import loadOrCreate from './load-or-create';

const { TWITTER_OAUTH_API_KEY, TWITTER_OAUTH_API_SECRET, TWITTER_OAUTH_URL } = config;

passport.use(
  new PassportTwitter.Strategy(
    {
      consumerKey: TWITTER_OAUTH_API_KEY,
      consumerSecret: TWITTER_OAUTH_API_SECRET,
      callbackURL: TWITTER_OAUTH_URL,
      passReqToCallback: true,
    },
    async (req, token, tokenSecret, profile, done) => {
      const user = await loadOrCreate({
        user: req.user,
        profileId: profile.id,
        idColumn: 'twitterId',
        avatarColumn: 'twitterAvatar',
        avatarUrl: profile.photos[0].value,
      });
      done(null, user);
    },
  ),
);
