import passport from 'passport';
import './google';
import './twitter';
import './facebook';
import './discord';
import { models } from '../../data/db';

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const user = await models().user.findById(id);
  done(null, user);
});

export default passport;

export const requireAuthentication = (req, res, next) =>
  req.isAuthenticated() ? next() : res.redirect('/');
