import redis from 'redis';
import connectRedis from 'connect-redis';
import session from 'express-session';
import createLogger from 'debug';
import config from '../config';

const { REDIS_HOST, REDIS_PORT, SESSION_SECRET } = config;

let connection;
const log = createLogger('redis');

export const connect = () =>
  new Promise((resolve, reject) => {
    try {
      connection = redis.createClient({
        port: REDIS_PORT,
        host: REDIS_HOST,
        no_ready_check: true,
        retry_strategy(options) {
          log(`Retry ${options.attempt}`);
          log(`Total time ${options.total_retry_time}`);

          if (options.total_retry_time > 1000 * 60) {
            return new Error('Retry time exhausted');
          }
          if (options.attempt > 10) {
            return new Error("Can't connect after 10 attempts");
          }
          return Math.min(options.attempt * 100, 1000);
        },
      });
      connection.on('error', err => {
        log('Redis: Connection Error: ', err);
        reject(err);
      });
      connection.on('ready', () => {
        log('Redis: Connected!');
        resolve();
      });
    } catch (err) {
      log('Redis: Connection Error: ', err);
      reject(err);
    }
  });

export const middleware = () => {
  const RedisStore = connectRedis(session);
  return session({
    store: new RedisStore({
      client: connection,
    }),
    secret: SESSION_SECRET,
    resave: false,
  });
};
