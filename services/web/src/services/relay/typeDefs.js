export default `
    scalar DateTime

    interface Node {
      id: ID!
    }

    type Query {
      me: User,
      user(username: String!): User,
      game(id: String!): Game,
      node(id: ID!): Node
    }

    type Tag implements Node {
      id: ID!
      name: String,
    }

    type User implements Node {
      id: ID!
      username: String,
      avatar: String,
      avatarUrl: String,
      tags: [Tag]
      hasGoogle: Boolean,
      hasTwitter: Boolean,
      hasFacebook: Boolean,
      hasDiscord: Boolean,
      hasPublicProfile: Boolean,
      hasEmailNotifications: Boolean,
      hasPublicPlayInvites: Boolean,
      googleAvatar: String,
      twitterAvatar: String,
      facebookAvatar: String,
      discordAvatar: String,
      playedGames: [Game]
    }

    type Game implements Node {
      id: ID!
      name: String,
      art: String,
      rating: Float,
      lastPlayedAt: DateTime,
      featured: Boolean,
      players: [User]
    }
`;
