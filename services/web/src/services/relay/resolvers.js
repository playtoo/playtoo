import isoDate from 'graphql-iso-date';
import Sequelize from 'sequelize';

// import { getRandomFn } from '../../data/db';
import * as db from '../../data/db';

class Tag {
  constructor({ id, name }) {
    this.id = id;
    this.name = name;
  }
}

class Game {
  constructor(dbGame) {
    this.id = dbGame.id;
    this.name = dbGame.name;
    this.art = dbGame.art;
    this.rating = dbGame.rating;
    this.lastPlayedAt = dbGame.lastPlayedAt;
    this.featured = dbGame.featured;
  }
}

class User {
  constructor(dbUser) {
    this.dbUser = dbUser;
    this.username = dbUser.username;
    this.id = dbUser.id;
    this.username = dbUser.username;
    this.avatar = dbUser.avatar;
    this.hasGoogle = Boolean(dbUser.googleId);
    this.hasTwitter = Boolean(dbUser.twitterId);
    this.hasFacebook = Boolean(dbUser.facebookId);
    this.hasDiscord = Boolean(dbUser.discordId);
    this.hasPublicProfile = Boolean(dbUser.publicProfile);
    this.hasEmailNotifications = Boolean(dbUser.emailNotifications);
    this.hasPublicPlayInvites = Boolean(dbUser.publicPlayInvites);
    this.googleAvatar = dbUser.googleAvatar;
    this.twitterAvatar = dbUser.twitterAvatar;
    this.facebookAvatar = dbUser.facebookAvatar;
    this.discordAvatar = dbUser.discordAvatar;
    this.avatarUrl = (userToLoad => {
      switch (userToLoad.avatar) {
        case 'google':
          return userToLoad.googleAvatar;
        case 'twitter':
          return userToLoad.twitterAvatar;
        case 'facebook':
          return userToLoad.facebookAvatar;
        case 'discord':
          return userToLoad.discordAvatar;
        default:
          return null;
      }
    })(dbUser);
  }

  async tags() {
    const tags = await this.dbUser.getTags();
    return tags.map(t => new Tag({ id: t.tag }));
  }

  async playedGames() {
    const games = await this.dbUser.getGames();
    return games.map(game => new Game(game));
  }
}

export default {
  me: async (vars, { authenticatedUser }) => new User(authenticatedUser),
  user: async ({ username }) => new User(await db.models().user.findOne({ where: { username } })),
  game: async ({ id }) => new Game(await db.models().game.findOne({ where: { id } })),
};

// export default models => ({
//   DateTime: isoDate.GraphQLDateTime,
//   Query: {
//     me: async (obj, vars, { authenticatedUser }) => authenticatedUser,
//     user: async (obj, { username }) => models.user.findOne({ where: { username } }),
//     game: async (obj, { id }) => models.game.findOne({ where: { slug: id } }),
//     featuredGames: async (obj, { limit = 3 }) =>
//       models.game.findAll({
//         where: { featured: true },
//         order: [[getRandomFn()]],
//         limit,
//       }),
//     allGames: async (obj, { limit = 10, offset = 0, search = '' }) => {
//       const { rows, count } = await models.game.findAndCount({
//         where: {
//           name: {
//             [Sequelize.Op.iLike]: `%${search}%`,
//           },
//         },
//         order: [['name', 'ASC']],
//         limit,
//         offset,
//       });
//       return {
//         games: rows,
//         meta: {
//           total: count,
//           hasMore: count > limit + offset,
//         },
//       };
//     },
//   },
//   User: {
//     tags: async userToLoad => {
//       const tags = await userToLoad.getTags();
//       return tags.map(t => ({ id: t.tag }));
//     },
//     avatarUrl: async userToLoad => {
//       switch (userToLoad.avatar) {
//         case 'google':
//           return userToLoad.googleAvatar;
//         case 'twitter':
//           return userToLoad.twitterAvatar;
//         case 'facebook':
//           return userToLoad.facebookAvatar;
//         case 'discord':
//           return userToLoad.discordAvatar;
//         default:
//           return null;
//       }
//     },
//     hasGoogle: async userToLoad => Boolean(userToLoad.googleId),
//     hasTwitter: async userToLoad => Boolean(userToLoad.twitterId),
//     hasFacebook: async userToLoad => Boolean(userToLoad.facebookId),
//     hasDiscord: async userToLoad => Boolean(userToLoad.discordId),
//     hasPublicProfile: async userToLoad => Boolean(userToLoad.publicProfile),
//     hasEmailNotifications: async userToLoad => Boolean(userToLoad.emailNotifications),
//     hasPublicPlayInvites: async userToLoad => Boolean(userToLoad.publicPlayInvites),
//     googleAvatar: async userToLoad => userToLoad.googleAvatar,
//     twitterAvatar: async userToLoad => userToLoad.twitterAvatar,
//     facebookAvatar: async userToLoad => userToLoad.facebookAvatar,
//     discordAvatar: async userToLoad => userToLoad.discordAvatar,

//     playedGames: async userToLoad => {
//       const games = await userToLoad.getGames();
//       return games.map(game => ({
//         name: game.name,
//         lastPlayedAt: game.usergame.lastPlayed,
//         art: game.art,
//       }));
//     },
//   },

//   Mutation: {
//     setUsername: async (obj, { username }, { authenticatedUser }) => {
//       // TODO check that username is unique
//       if (!authenticatedUser) throw new Error('Auth required');
//       return authenticatedUser.update({
//         username,
//       });
//     },
//     addTagToUser: async (obj, { tag }, { authenticatedUser }) => {
//       if (!authenticatedUser) throw new Error('Auth required');
//       const instance = await models.tag.create({ tag });
//       await authenticatedUser.addTag(instance);
//       return authenticatedUser;
//     },
//     removeTagFromUser: async (obj, { tag }, { authenticatedUser }) => {
//       if (!authenticatedUser) throw new Error('Auth required');

//       const tagToRemove = await models.tag.findOne({
//         where: {
//           userId: authenticatedUser.id,
//           tag,
//         },
//       });
//       await tagToRemove.destroy();
//       return authenticatedUser.reload();
//     },
//     setPublicProfile: async (object, { isPublic }, { authenticatedUser }) => {
//       if (!authenticatedUser) throw new Error('Auth required');
//       return authenticatedUser.update({
//         publicProfile: isPublic,
//       });
//     },
//     setEmailNotifications: async (object, { isActive }, { authenticatedUser }) => {
//       if (!authenticatedUser) throw new Error('Auth required');
//       return authenticatedUser.update({
//         emailNotifications: isActive,
//       });
//     },
//     setPublicPlayInvites: async (object, { isPublic }, { authenticatedUser }) => {
//       if (!authenticatedUser) throw new Error('Auth required');
//       return authenticatedUser.update({
//         publicPlayInvites: isPublic,
//       });
//     },
//     setAvatar: async (object, { provider }, { authenticatedUser }) => {
//       if (!authenticatedUser) throw new Error('Auth required');
//       if (
//         provider !== 'google' &&
//         provider !== 'facebook' &&
//         provider !== 'twitter' &&
//         provider !== 'discord'
//       )
//         return null;

//       return authenticatedUser.update({
//         avatar: provider,
//       });
//     },
//   },
// });
