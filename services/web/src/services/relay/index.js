import grapqhl from './grapqhl-cjs';
import typeDefs from './typeDefs';

const { buildSchema } = grapqhl;
export const schema = buildSchema(typeDefs);

console.log(schema);

export { default as resolvers } from './resolvers';
