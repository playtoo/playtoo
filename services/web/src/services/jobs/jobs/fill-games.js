import igdb from '../../igdb';

const name = 'fill-games';
const title = 'Filling games table';

const creator = queue =>
  new Promise((resolve, reject) => {
    const job = queue.create(name, { title }).unique(name);

    queue.every('1 day', job);

    job.save(err => (err ? reject(err) : resolve(true)));
  });

const installResolver = (queue, models) => {
  const { game: gameModel } = models();

  queue.process(name, async (job, done) => {
    try {
      await Promise.all(
        Array.from(Array(5).keys()).map(page =>
          igdb
            .games({
              fields: 'id,name,slug,popularity,total_rating,game_modes,cover',
              limit: 50,
              offset: 50 * page,
              order: 'popularity:desc',
              'filter[game_modes][eq]': 2,
            })
            .then(result =>
              Promise.all(
                result.body.map(game => {
                  let coverUrl;

                  if (game.cover) {
                    const size = 'cover_big';
                    const hash = game.cover.cloudinary_id;
                    coverUrl = `https://images.igdb.com/igdb/image/upload/t_${size}/${hash}.jpg`;
                  } else {
                    coverUrl = '';
                  }

                  return gameModel.upsert({
                    igdbId: String(game.id),
                    name: game.name,
                    art: coverUrl,
                    popularity: game.popularity,
                    rating: game.total_rating,
                    slug: game.slug,
                  });
                }),
              ),
            ),
        ),
      );
      done();
    } catch (e) {
      done(e);
    }
  });
};

export default { creator, installResolver };
