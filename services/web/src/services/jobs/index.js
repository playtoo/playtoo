import kue from 'kue-scheduler';
import config from '../../config';
import fillGamesJob from './jobs/fill-games';

const { REDIS_HOST, REDIS_PORT } = config;
const q = kue.createQueue({
  prefix: 'q',
  redis: {
    port: REDIS_PORT,
    host: REDIS_HOST,
    db: 2,
  },
});

const enqueJob = jobType => {
  switch (jobType) {
    case 'fill-games':
      return fillGamesJob.creator(q);

    default:
      throw new Error('Unkonwn job type');
  }
};

export const fillGames = () => enqueJob('fill-games');

export const installResolvers = models => {
  fillGamesJob.installResolver(q, models);
};

export const { app } = kue;
