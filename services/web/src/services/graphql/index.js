import graphqlTools from 'graphql-tools';
import apolloServer from 'apollo-server-express';

import typeDefs from './typeDefs';
import resolvers from './resolvers';
import * as db from '../../data/db';

let schema;

const getSchema = () => {
  if (!schema) {
    schema = graphqlTools.makeExecutableSchema({
      typeDefs,
      resolvers: resolvers(db.models()),
    });
  }
  return schema;
};

export const graphqlHandler = contextFactory => {
  const r = apolloServer.graphqlExpress(req => ({
    schema: getSchema(),
    context: contextFactory(req),
  }));
  return r;
};

export const graphiqlHandler = url => apolloServer.graphiqlExpress({ endpointURL: url });
