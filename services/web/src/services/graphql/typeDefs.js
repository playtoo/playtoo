export default `
    scalar DateTime

    type Query {
      me: User,
      user(username: String!): User,
      game(id: String!): Game,
      featuredGames(limit: Int!): [Game],
      allGames(limit: Int, offset: Int, search: String): GameResult,
    }

    type GameResult {
      games: [Game],
      meta: SearchResult
    }

    type SearchResult {
      total: Int!,
      hasMore: Boolean!
    }

    type Tag {
      id: String!,
      name: String,
    }

    type Mutation {
      setUsername(username: String!): User,
      addTagToUser(tag: String!): User,
      removeTagFromUser(tag: String!): User,
      setPublicProfile(isPublic: Boolean!): User,
      setEmailNotifications(isActive: Boolean!): User,
      setPublicPlayInvites(isPublic: Boolean!): User,
      setAvatar(provider:String!): User,
    }

    type User {
      id: String,
      username: String,
      avatar: String,
      avatarUrl: String,
      tags: [Tag]
      hasGoogle: Boolean,
      hasTwitter: Boolean,
      hasFacebook: Boolean,
      hasDiscord: Boolean,
      hasPublicProfile: Boolean,
      hasEmailNotifications: Boolean,
      hasPublicPlayInvites: Boolean,
      googleAvatar: String,
      twitterAvatar: String,
      facebookAvatar: String,
      discordAvatar: String,
      playedGames: [Game]
    }

    type Game {
      id: String,
      name: String,
      art: String,
      rating: Float,
      lastPlayedAt: DateTime,
      featured: Boolean,
      players: [User]
    }
`;
