const keys = [
  'DB_USER',
  'DB_PASS',
  'DB_NAME',
  'POSTGRESQL_HOST',
  'POSTGRESQL_PORT',
  'REDIS_HOST',
  'REDIS_PORT',
  'SESSION_SECRET',
  'GOOGLE_OAUTH_CLIENT_ID',
  'GOOGLE_OAUTH_CLIENT_SECRET',
  'GOOGLE_OAUTH_URL',
  'TWITTER_OAUTH_API_KEY',
  'TWITTER_OAUTH_API_SECRET',
  'TWITTER_OAUTH_URL',
  'FACEBOOK_OAUTH_CLIENT_ID',
  'FACEBOOK_OAUTH_CLIENT_SECRET',
  'FACEBOOK_OAUTH_URL',
  'DISCORD_OAUTH_CLIENT_ID',
  'DISCORD_OAUTH_CLIENT_SECRET',
  'DISCORD_OAUTH_URL',
  'HTTP_WEB_PORT',
  'HTTPS_WEB_PORT',
  'HTTP_JOBS_PORT',
  'HTTPS_JOBS_PORT',
  'SSL_CERT_PATH',
  'IGDB_API_KEY',
];

const config = {};

keys.forEach(key => {
  if (!process.env[key]) throw new Error(`Can't find value for ${key}`);
  config[key] = process.env[key];
});

export default config;
