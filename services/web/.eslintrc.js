module.exports = {
  root: true,

  extends: ['airbnb-base', 'plugin:prettier/recommended'],

  env: {
    browser: false,
    node: true,
    es6: true,
  },

  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module',
  },

  rules: {
    'no-restricted-syntax': 'off',
    'no-continue': 'off',
    'no-console': 'off',
    'no-debugger': 'off',
    'no-await-in-loop': 'off',
  },
};
