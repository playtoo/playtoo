#!/bin/sh
set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

export ESM_OPTIONS="{ mode: 'auto', cjs: { vars: true }}"
export DEBUG="*, -stylus:*, -express:*, -send, -sequelize:*, -express-session, -connect:*"
export DEBUG_COLORS=true

./node_modules/.bin/supervisor \
  --watch src \
  -- \
    -r esm \
    "--inspect=0.0.0.0:${DEBUG_JOBS_PORT}" \
    ./src/jobs.js