#!/bin/sh
set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

export ESM_OPTIONS="{ mode: 'auto', cjs: { vars: true }, sourceMap: true, debug: true, cache:false,warnings:true}"
export DEBUG="*, -spdy:*, -body-parser:json, -express:*, -send, -sequelize:*, -express-session, -connect:*"

./node_modules/.bin/supervisor \
  --watch src \
  -- \
    -r esm \
    "--inspect=0.0.0.0:${DEBUG_WEB_PORT}" \
    ./src/web.js