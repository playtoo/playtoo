#!/bin/sh
set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

export ESM_OPTIONS="{ mode: 'auto', cjs: { vars: true }}"
export DEBUG=""

./node_modules/.bin/supervisor \
  --ignore . \
  -- \
    -r esm \
    ./src/web.js