#!/bin/sh
set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

export ESM_OPTIONS="{ mode: 'auto', cjs: { vars: true }}"
export DEBUG="*, -stylus:*, -express:*, -send, -sequelize:*, -express-session, -connect:*"
export DEBUG_COLORS=true

./node_modules/.bin/supervisor \
  --ignore . \
    -r esm \
    ./src/jobs.js