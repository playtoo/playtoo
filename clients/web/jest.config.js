module.exports = {
  verbose: true,
  moduleFileExtensions: ['js', 'jsx'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  setupFiles: ['./test-setup/enzyme.js'],
};
