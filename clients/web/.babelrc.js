module.exports = {
  presets: [
    '@babel/react',
    [
      '@babel/env',
      {
        modules: false,
        useBuiltIns: 'entry',
      },
    ],
  ],
  plugins: [
    '@babel/plugin-proposal-decorators',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
  ],
  // env: {
  //   test: {
  //     plugins: ['transform-es2015-modules-commonjs'],
  //   },
  // },
};
