module.exports = {
  root: true,

  parser: 'babel-eslint',

  extends: ['airbnb', 'plugin:prettier/recommended', 'plugin:jest/recommended'],

  env: {
    browser: true,
    es6: true,
  },

  parserOptions: {
    ecmaVersion: 2017,
    ecmaFeatures: {
      jsx: true,
    },
    sourceType: 'module',
  },

  rules: {
    'no-restricted-syntax': 'off',
    'no-continue': 'off',
    'no-console': 'off',
    'no-debugger': 'off',
    'no-unused-expressions': 'off',
    'jsx-a11y/label-has-for': 'off',
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: [
          '**/story.jsx',
          '**/test-setup/*.js',
          '**/test.jsx',
          '**/src/lib/storybook.jsx',
          'webpack.config.dev.js',
          'webpack.config.prod.js',
        ],
      },
    ],
  },

  overrides: [
    {
      files: ['story.jsx'],
      rules: {
        'no-param-reassign': 'off',
      },
    },
  ],
};
