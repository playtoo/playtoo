module.exports = {
  devtool: 'source-map',

  entry: {
    main: ['./src/main.jsx'],
  },

  resolve: {
    mainFields: ['browserify', 'browser', 'module', 'main'],
    extensions: ['.js', '.jsx'],
  },

  module: {
    rules: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /.css?$/,
        loader: ['style-loader', 'css-loader'],
      },
      {
        test: /\.svg$/,
        loader: 'raw-loader',
      },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2)(\?.*)?$/,
        loader: 'file-loader',
      },
    ],
  },
};
