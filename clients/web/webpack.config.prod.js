const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const { NamedModulesPlugin } = require('webpack');
const HashOutput = require('webpack-plugin-hash-output');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const baseConfig = require('./webpack.config');

module.exports = Object.assign({}, baseConfig, {
  mode: 'production',

  output: {
    filename: '[name].[chunkhash].js',
    sourceMapFilename: '[name].[chunkhash].js.map',
    path: path.resolve(__dirname, '../../services/web/static'),
    publicPath: '/',
  },

  optimization: {
    namedModules: true,
    concatenateModules: true,
  },

  plugins: [
    new UglifyJsPlugin({
      sourceMap: true,
      cache: true,
      parallel: 4,
    }),
    new HashOutput(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
  ],
});
