const path = require('path');

module.exports = {
  mode: 'development',

  resolve: {
    mainFields: ['main'],
    extensions: ['.js', '.jsx'],
  },

  optimization: {
    namedModules: true,
    concatenateModules: true,
  },

  module: {
    rules: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.svg$/,
        loader: 'raw-loader',
      },
      {
        test: /.css?$/,
        loader: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2)(\?.*)?$/,
        loader: 'file-loader',
      },
    ],
  },
};
