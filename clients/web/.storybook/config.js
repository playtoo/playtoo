import React from 'react';
import { configure, addDecorator, setAddon } from '@storybook/react';
import { injectGlobal } from 'styled-components';
import clone from 'clone';

import avatar from './static/avatar.jpg';

addDecorator(story => {
  require('bootstrap');
  require('jquery/dist/jquery.slim.js');
  require('popper.js');
  require('../src/style');

  return story();
});

const req = require.context('../src/view', true, /story\.jsx$/);

function loadStories() {
  req.keys().forEach(req);
}

configure(loadStories, module);
