import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

import FormField from '../form-field';

const Loading = ({ className }) => (
  <FormField
    className={className}
    label="Your tags"
    help="Describe yourself to other players"
    field={<Select name="tags" options={[]} placeholder="Loading..." />}
  />
);

Loading.propTypes = {
  className: PropTypes.string,
};

Loading.defaultProps = {
  className: '',
};

export default styled(Loading)`
  & {
    display: flex;
    flex-direction: column;
  }
`;
