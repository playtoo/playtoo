import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import UserTag from '../user-tags';

import FormField from '../form-field';

const EditTags = ({ className, addTag, tags }) => {
  const values = tags.map(({ id, name }) => ({ value: id, label: name }));
  return (
    <FormField
      className={className}
      label="Your tags"
      help="Describe yourself to other players"
      field={<Select name="tags" onChange={({ value }) => addTag(value)} options={values} />}
      secondaryField={<UserTag edit />}
    />
  );
};

EditTags.propTypes = {
  addTag: PropTypes.func.isRequired,
  className: PropTypes.string,
  tags: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

EditTags.defaultProps = {
  className: '',
};

export default styled(EditTags)`
  & {
    display: flex;
    flex-direction: column;
  }
`;
