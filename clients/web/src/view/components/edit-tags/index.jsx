import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import View from './view';
import Loading from './loading';
import { FRAGMENT } from '../user-tags';

const computeUnusedTags = (userTags, allTags) => {
  const userTagsIds = userTags.map(t => t.id);
  return allTags.filter(({ id }) => !userTagsIds.includes(id));
};

const SET_TAG = gql`
  mutation addTagToUser($tag: String!) {
    addTagToUser(tag: $tag) {
      ...UserTags
    }
  }
  ${FRAGMENT}
`;

const GET_TAGS = gql`
  {
    me {
      ...UserTags
    }
    tags @client {
      id
      name
    }
  }
  ${FRAGMENT}
`;

export default compose(graphql(GET_TAGS), graphql(SET_TAG))(
  ({ data: { loading, me, error, tags }, mutate }) => {
    if (loading) return <Loading />;
    if (error) throw error;

    return (
      <View
        tags={computeUnusedTags(me.tags, tags)}
        addTag={tag => mutate({ variables: { tag } })}
      />
    );
  },
);
