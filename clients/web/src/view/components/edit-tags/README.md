# Edit Tags

This component is used on the Edit Profile screens, to select the tags that represents the user.

The select field shows the tags in no particular oder. The user can type to filter the list. When
an item is selected, it gets added to the list of tags, saved into the database and removed from the
selecte.

The user can also click on the tag to remove it. When this happens, it gets removed form the
database automatically and available again to be selected.

# TODO

* Do we want to limit the list?
* Do we want to order the list?
* If not, what do we show on the Select when there are no more tags?
* A design for removing a tag
* Do we _not_ having any tag?
* Come up with the list of tags
