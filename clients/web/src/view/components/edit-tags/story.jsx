import readme from './README.md';
import EditTags from './index';
import { storiesOf, withApollo } from '../../../lib/storybook';
import { rootValue } from '../../../services/graphql';

const Mutation = {
  setPublicProfile: (obj, { isPublic }) => ({
    id: 1,
    hasPublicProfile: isPublic,
  }),
};

storiesOf('EditTags', readme, module)
  .add('when loading', () =>
    withApollo({
      Query: {
        me: async () => new Promise(() => {}),
      },
      Mutation,
    })(EditTags),
  )
  .add('with no tags', () =>
    withApollo({
      Query: {
        me: {
          id: 1,
          tags: [],
        },
      },
      Mutation,
    })(EditTags),
  )
  .add('with some tags selected', () =>
    withApollo({
      Query: {
        me: {
          id: 1,
          tags: [
            { id: 'chill' },
            { id: 'support' },
            { id: 'neutral' },
            { id: 'mage' },
            { id: 'achiever' },
          ],
        },
      },
      Mutation,
    })(EditTags),
  )
  .add('with all tags selected', () =>
    withApollo({
      Query: {
        me: {
          id: 1,
          tags: rootValue.tags.map(t => t.id),
        },
      },
      Mutation,
    })(EditTags),
  );

// .decorateWithStore(({ actions, store }) => {
//   actions.addUserTag = tag => {
//     store.user.tags.push(tag);
//   };

//   actions.removeUserTag = tag => {
//     const index = store.user.tags.indexOf(tag);
//     if (index >= 0) {
//       store.user.tags.splice(index, 1);
//     }
//   };
// })
// .addWithStore('default', withNotes(readme)(() => <EditTags />))

// .addWithStore('all tags selected', ({ store }) => {
//   store.user.tags.push('chill');
//   store.user.tags.push('goofy');
//   store.user.tags.push('support');
//   store.user.tags.push('role-player');
//   store.user.tags.push('mage');
//   store.user.tags.push('tank');
//   store.user.tags.push('dps');
//   store.user.tags.push('healer');
//   store.user.tags.push('lawful-good');
//   store.user.tags.push('neutral-good');
//   store.user.tags.push('chaotic-good');
//   store.user.tags.push('lawful-neutral');
//   store.user.tags.push('neutral');
//   store.user.tags.push('chaotic-neutral');
//   store.user.tags.push('lawful-evil');
//   store.user.tags.push('neutral-evil');
//   store.user.tags.push('chaotic-evil');
//   store.user.tags.push('killer');
//   store.user.tags.push('achiever');
//   store.user.tags.push('explorer');
//   store.user.tags.push('socializer');
//   return <EditTags />;
// });
