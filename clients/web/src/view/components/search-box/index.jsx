import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import compose from '../../../lib/compose';

const SEARCH = gql`
  query {
    gameSearch @client
  }
`;

const CHANGE = gql`
  mutation setGameSearch($search: String!) {
    setGameSearch(search: $search) @client
  }
`;

const SearchBox = ({ search, change }) => (
  <div>
    <input
      type="text"
      placeholder="Search"
      value={search}
      onChange={ev => change(ev.target.value)}
    />
  </div>
);
SearchBox.propTypes = {
  search: PropTypes.string,
  change: PropTypes.func.isRequired,
};
SearchBox.defaultProps = {
  search: '',
};

export default compose(graphql(SEARCH), graphql(CHANGE))(({ data: { gameSearch }, mutate }) => (
  <SearchBox
    search={gameSearch}
    change={newSearch => mutate({ variables: { search: newSearch } })}
  />
));
