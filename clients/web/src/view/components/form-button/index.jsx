import React from 'react';
import PropTypes from 'prop-types';
import classname from 'classnames';

import styled from 'styled-components';

const FormButton = ({ className, active, onClick, children }) => (
  <button className={classname(className, { active })} type="button" onClick={() => onClick()}>
    {children}
  </button>
);

FormButton.propTypes = {
  className: PropTypes.string,
  active: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

FormButton.defaultProps = {
  className: '',
  active: false,
};

export default styled(FormButton)`
  #root & {
    color: rgb(182, 185, 185);
    text-transform: uppercase;
    background-color: rgb(237, 237, 237);

    border: 0;
    border-bottom: 1px solid #b9b9b9;

    margin-right: 20px;

    border-radius: 0px;
    height: 48px;
    min-width: 68px;
    padding: 6px 18px 6px 18px;
    display: inline-block;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    user-select: none;
    font-size: 16px;
    line-height: 24px;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
      border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

    &:last-child {
      margin-right: 0px;
    }

    &.active {
      color: white;
      background-color: #c93833;
    }
  }
`;
