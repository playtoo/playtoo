import React from 'react';
import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import { action } from '@storybook/addon-actions';

import readme from './README.md';
import FormButton from './index';

storiesOf('FormButton', module)
  .add('default', withNotes(readme)(() => <FormButton onClick={action('click')}>Label</FormButton>))
  .add('active', () => (
    <FormButton onClick={action('click')} active>
      Label
    </FormButton>
  ));
