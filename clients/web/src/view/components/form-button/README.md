# Edit Username

This component is used on the Edit Profile screen, to select the username.

This component automatically checks if the username is available when the user is typing (afte they
stop typing for 300ms). If it is available, a save button is shown and the field is rendered with a
green border.. If it is not available, it will show a red border.

For the Story simulation, we'll show the checking state for 3 seconds. Then, if the entered user is
'Superman', it will mark the field as invalid. It will be valid with any other input.

# TODO

* Check for variations: 'Superman' vs 'superman' should be the same user
* Check that the name can be transformed into a url (eg. remove spaces and symbols)
* Set a minimum and maximum size
* Show a message saying _why_ it is not available
