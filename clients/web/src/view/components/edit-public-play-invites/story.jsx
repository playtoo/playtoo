import readme from './README.md';
import EditPublicPlayInvites from './index';
import { storiesOf, withApollo } from '../../../lib/storybook';

const Mutation = {
  setPublicPlayInvites: (obj, { isPublic }) => ({
    id: 1,
    hasPublicPlayInvites: isPublic,
  }),
};

storiesOf('EditPublicPlayInvites', readme, module)
  .add('when loading', () =>
    withApollo({
      Query: {
        me: async () => new Promise(() => {}),
      },
      Mutation,
    })(EditPublicPlayInvites),
  )

  .add('with "Anyone" selected', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          hasPublicPlayInvites: true,
        }),
      },
      Mutation,
    })(EditPublicPlayInvites),
  )

  .add('with "Just my friends" selected', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          hasPublicPlayInvites: false,
        }),
      },
      Mutation,
    })(EditPublicPlayInvites),
  );
