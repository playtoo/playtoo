import React from 'react';
import PropTypes from 'prop-types';

import FormField from '../form-field';
import FormButton from '../form-button';

const EditPublicPlayInvites = ({ className, setPublicPlayInvites, hasPublicPlayInvites }) => (
  <FormField
    className={className}
    label="Who can send play invites?"
    help="Who can send play invites?"
    secondaryField={
      <div className="buttons">
        <FormButton
          active={hasPublicPlayInvites === true}
          onClick={() => setPublicPlayInvites(true)}
        >
          Anyone
        </FormButton>
        <FormButton
          active={hasPublicPlayInvites === false}
          onClick={() => setPublicPlayInvites(false)}
        >
          Just my friends
        </FormButton>
      </div>
    }
  />
);

EditPublicPlayInvites.propTypes = {
  setPublicPlayInvites: PropTypes.func.isRequired,
  hasPublicPlayInvites: PropTypes.bool,
  className: PropTypes.string,
};

EditPublicPlayInvites.defaultProps = {
  hasPublicPlayInvites: true,
  className: '',
};

export default EditPublicPlayInvites;
