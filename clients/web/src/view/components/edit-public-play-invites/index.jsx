import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import View from './view';

export const FRAGMENT = gql`
  fragment UserPublicPlayInvites on User {
    id
    hasPublicPlayInvites
  }
`;

const SET_PUBLIC_PLAY_INVITES = gql`
  mutation setPublicPlayInvites($isPublic: Boolean!) {
    setPublicPlayInvites(isPublic: $isPublic) {
      ...UserPublicPlayInvites
    }
  }
  ${FRAGMENT}
`;

const GET_PUBLIC_PLAY_INVITES = gql`
  {
    me {
      ...UserPublicPlayInvites
    }
  }
  ${FRAGMENT}
`;

export default compose(graphql(GET_PUBLIC_PLAY_INVITES), graphql(SET_PUBLIC_PLAY_INVITES))(
  ({ data: { loading, me, error }, mutate }) => {
    if (loading) return <div>Loading...</div>;
    if (error) throw error;

    return (
      <View
        hasPublicPlayInvites={me.hasPublicPlayInvites}
        setPublicPlayInvites={isPublic => mutate({ variables: { isPublic } })}
      />
    );
  },
);
