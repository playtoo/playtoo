# Edit Email Notifications

This component is used on the Edit Profile screens, to select the preference about email
notifications.

It shows two buttons to enable and disable the setting.
