import React from 'react';
import PropTypes from 'prop-types';

import FormField from '../form-field';
import FormButton from '../form-button';

const EditEmailNotifications = ({ className, setEmailNotifications, hasEmailNotifications }) => (
  <FormField
    className={className}
    label="Do you want email notifications?"
    help="Just friend request and play sessions"
    secondaryField={
      <div className="buttons">
        <FormButton
          active={hasEmailNotifications === true}
          onClick={() => setEmailNotifications(true)}
        >
          Sure
        </FormButton>
        <FormButton
          active={hasEmailNotifications === false}
          onClick={() => setEmailNotifications(false)}
        >
          No
        </FormButton>
      </div>
    }
  />
);

EditEmailNotifications.propTypes = {
  setEmailNotifications: PropTypes.func.isRequired,
  hasEmailNotifications: PropTypes.bool,
  className: PropTypes.string,
};

EditEmailNotifications.defaultProps = {
  hasEmailNotifications: true,
  className: '',
};

export default EditEmailNotifications;
