import readme from './README.md';
import EditEmailNotifications from './index';
import { storiesOf, withApollo } from '../../../lib/storybook';

const Mutation = {
  setPublicProfile: (obj, { isActive }) => ({
    id: 1,
    hasEmailNotifications: isActive,
  }),
};

storiesOf('EditEmailNotifications', readme, module)
  .add('when loading', () =>
    withApollo({
      Query: {
        me: async () => new Promise(() => {}),
      },
      Mutation,
    })(EditEmailNotifications),
  )

  .add('with "Sure" selected', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          hasEmailNotifications: true,
        }),
      },
      Mutation,
    })(EditEmailNotifications),
  )

  .add('with "No" selected', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          hasEmailNotifications: false,
        }),
      },
      Mutation,
    })(EditEmailNotifications),
  );
