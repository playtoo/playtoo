import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import View from './view';

export const FRAGMENT = gql`
  fragment UserEmailNotifications on User {
    id
    hasEmailNotifications
  }
`;

const SET_EMAIL_NOTIFICATIONS = gql`
  mutation setEmailNotifications($isActive: Boolean!) {
    setEmailNotifications(isActive: $isActive) {
      ...UserEmailNotifications
    }
  }
  ${FRAGMENT}
`;

const GET_EMAIL_NOTIFICATIONS = gql`
  {
    me {
      ...UserEmailNotifications
    }
  }
  ${FRAGMENT}
`;

export default compose(graphql(GET_EMAIL_NOTIFICATIONS), graphql(SET_EMAIL_NOTIFICATIONS))(
  ({ data: { loading, error, me }, mutate }) => {
    if (loading) return <div>Loading...</div>;
    if (error) throw error;

    return (
      <View
        hasEmailNotifications={me.hasEmailNotifications}
        setEmailNotifications={isActive => mutate({ variables: { isActive } })}
      />
    );
  },
);
