import React from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import styled from 'styled-components';

import GameCard from '../game-card';

const AllGames = ({ className, games, loadMore, hasMore }) => (
  <InfiniteScroll
    className={className}
    pageStart={0}
    loadMore={loadMore}
    initialLoad={false}
    useWindow
    hasMore={hasMore}
    loader={
      <div key="loader" className="loader">
        Here!! Loading ...
      </div>
    }
  >
    {games.map(game => <GameCard key={game.id} name={game.name} image={game.art} />)}
  </InfiniteScroll>
);

AllGames.propTypes = {
  className: PropTypes.string.isRequired,
  games: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      art: PropTypes.string.isRequired,
    }),
  ).isRequired,
  loadMore: PropTypes.func.isRequired,
  hasMore: PropTypes.bool,
};

AllGames.defaultProps = {
  hasMore: false,
};

export default styled(AllGames)`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;
