import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { mergeWith } from 'lodash';
import compose from '../../../lib/compose';

import View from './view';

const GET_GAMES = gql`
  query allGames($offset: Int, $limit: Int, $search: String) {
    allGames(offset: $offset, limit: $limit, search: $search) {
      games {
        id
        name
        art
      }
      meta {
        hasMore
      }
    }
  }
`;

const SEARCH = gql`
  query {
    gameSearch @client
  }
`;

export default compose(
  graphql(SEARCH),
  graphql(GET_GAMES, {
    options: ({ data: { gameSearch } }) => ({
      variables: {
        search: gameSearch,
        limit: 15,
        offset: 0,
      },
    }),
  }),
)(({ data: { loading, error, allGames, fetchMore } }) => {
  if (loading) return null;
  if (error) throw error;

  const { games, meta: { hasMore } } = allGames;
  return (
    <View
      games={games}
      loadMore={page => {
        fetchMore({
          variables: {
            offset: page * 15,
          },
          updateQuery: (prev, { fetchMoreResult }) =>
            mergeWith(
              {},
              prev,
              fetchMoreResult,
              (a, b) => (Array.isArray(a) ? a.concat(b) : undefined),
            ),
        });
      }}
      hasMore={hasMore}
    />
  );
});
