import React from 'react';
import styled from 'styled-components';

const Placeholder = styled.div`
  width: 128px;
  height: 128px;
  border-radius: 50%;
  background-color: #eee;
  margin-bottom: 30px;
`;

const EditAvatarLoading = () => (
  <div className="form-group">
    <Placeholder />
  </div>
);

export default EditAvatarLoading;
