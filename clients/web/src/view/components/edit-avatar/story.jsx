import EditAvatar from './index';
import readme from './README.md';
import { storiesOf, withApollo } from '../../../lib/storybook';

import avatar1 from '../../../../.storybook/static/avatar1.png';
import avatar2 from '../../../../.storybook/static/avatar2.png';
import avatar3 from '../../../../.storybook/static/avatar3.png';
import avatar4 from '../../../../.storybook/static/avatar4.png';

const Mutation = {
  setAvatar: (obj, { provider }) => ({
    id: 1,
    avatar: provider,
    googleAvatar: avatar1,
    twitterAvatar: avatar2,
    facebookAvatar: avatar3,
    discordAvatar: avatar4,
  }),
};

storiesOf('EditAvatar', readme, module)
  .add('when loading', () =>
    withApollo({
      Query: {
        me: async () => new Promise(() => {}),
      },
      Mutation,
    })(EditAvatar),
  )

  .add('with "Google" avatar', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          avatar: 'google',
          googleAvatar: avatar1,
          twitterAvatar: avatar2,
          facebookAvatar: avatar3,
          discordAvatar: avatar4,
        }),
      },
      Mutation,
    })(EditAvatar),
  )

  .add('with "Twitter" avatar', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          avatar: 'twitter',
          googleAvatar: avatar1,
          twitterAvatar: avatar2,
          facebookAvatar: avatar3,
          discordAvatar: avatar4,
        }),
      },
      Mutation,
    })(EditAvatar),
  )

  .add('with "Facebook" avatar', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          avatar: 'facebook',
          googleAvatar: avatar1,
          twitterAvatar: avatar2,
          facebookAvatar: avatar3,
          discordAvatar: avatar4,
        }),
      },
      Mutation,
    })(EditAvatar),
  )

  .add('with "Discord" avatar', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          avatar: 'discord',
          googleAvatar: avatar1,
          twitterAvatar: avatar2,
          facebookAvatar: avatar3,
          discordAvatar: avatar4,
        }),
      },
      Mutation,
    })(EditAvatar),
  )

  .add('with "Twitter" and "Google" avatar', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          avatar: 'discord',
          googleAvatar: null,
          twitterAvatar: null,
          facebookAvatar: avatar3,
          discordAvatar: avatar4,
        }),
      },
      Mutation,
    })(EditAvatar),
  );
