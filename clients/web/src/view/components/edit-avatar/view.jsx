import React from 'react';
import classname from 'classnames';
import Select from 'react-select';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import 'react-select/dist/react-select.css';

const EditAvatar = ({
  avatar,
  googleAvatar,
  twitterAvatar,
  facebookAvatar,
  discordAvatar,
  className,
  setAvatar,
}) => {
  const options = [];

  // Create options for <Select>
  if (googleAvatar) options.push({ value: 'google', label: 'Google' });
  if (twitterAvatar) options.push({ value: 'twitter', label: 'Twitter' });
  if (facebookAvatar) options.push({ value: 'facebook', label: 'Facebook' });
  if (discordAvatar) options.push({ value: 'discord', label: 'Discord' });

  // Pick the selected provider
  const avatarUrl = (() => {
    switch (avatar) {
      case 'google':
        return googleAvatar;
      case 'twitter':
        return twitterAvatar;
      case 'facebook':
        return facebookAvatar;
      case 'discord':
        return discordAvatar;
      default:
        return null;
    }
  })();

  return (
    <div className={classname(className, `form-group`)}>
      <img alt="avatar" src={avatarUrl} className="rounded-circle" />

      <Select
        name="avatars"
        className="select"
        clearable={false}
        onChange={option => setAvatar(option.value)}
        value={avatar}
        options={options}
      />
    </div>
  );
};

EditAvatar.propTypes = {
  className: PropTypes.string,
  googleAvatar: PropTypes.string,
  twitterAvatar: PropTypes.string,
  facebookAvatar: PropTypes.string,
  discordAvatar: PropTypes.string,
  avatar: PropTypes.string,
  setAvatar: PropTypes.func.isRequired,
};

EditAvatar.defaultProps = {
  googleAvatar: null,
  twitterAvatar: null,
  facebookAvatar: null,
  discordAvatar: null,
  avatar: null,
  className: '',
};

export default styled(EditAvatar)`
  #root & {
    display: flex;
    align-items: center;
    margin-bottom: 30px;

    img {
      width: 128px;
      height: 128px;
    }

    .select {
      min-width: 180px;
      margin: 23px;
    }
  }
`;
