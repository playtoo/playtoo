import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import View from './view';
import Loading from './loading';

export const FRAGMENT = gql`
  fragment UserAvatar on User {
    id
    avatar
    googleAvatar
    twitterAvatar
    facebookAvatar
    discordAvatar
  }
`;

const SET_AVATAR = gql`
  mutation setAvatar($provider: String!) {
    setAvatar(provider: $provider) {
      ...UserAvatar
    }
  }
  ${FRAGMENT}
`;

const GET_AVATAR = gql`
  {
    me {
      ...UserAvatar
    }
  }
  ${FRAGMENT}
`;

export default compose(graphql(GET_AVATAR), graphql(SET_AVATAR))(
  ({ data: { loading, me }, mutate }) => {
    if (loading) {
      return <Loading />;
    }
    return (
      <View
        setAvatar={avatar => mutate({ variables: { provider: avatar } })}
        googleAvatar={me.googleAvatar}
        twitterAvatar={me.twitterAvatar}
        facebookAvatar={me.facebookAvatar}
        discordAvatar={me.discordAvatar}
        avatar={me.avatar}
      />
    );
  },
);
