# Edit Avatar

This component is used on the Edit Profile screens, to select the avatar that represents the user.

It shows the selected avatar and a Select field to choose between the authenticated
methods (Facebook, Twitter, etc.). When a new provider is selected, the avatar will be automatically
saved to the DB.

# TODO

* Provide a default value
* What if the authenticated method doesn't exist?
* We should provide a default avatar
* Add other avatar providers like Gravatar.com
