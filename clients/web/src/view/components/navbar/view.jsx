import React from 'react';
import styled from 'styled-components';
import SVGInline from 'react-svg-inline';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import playTooLogo from '../../assets/logo.svg';
import NavigationDropdown from '../navigation-dropdown';

const NavItem = ({ id, activeItem, onNavigate, title }) => (
  <li
    className={classnames(
      {
        active: (activeItem === id) === true,
      },
      'nav-item',
      id,
    )}
  >
    <a
      className="nav-link"
      href="/players"
      onClick={ev => {
        ev.preventDefault();
        onNavigate();
      }}
    >
      {title}
    </a>
  </li>
);

NavItem.propTypes = {
  id: PropTypes.string.isRequired,
  activeItem: PropTypes.string,
  onNavigate: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};
NavItem.defaultProps = {
  activeItem: null,
};

const NavBar = ({
  className,
  activeItem,
  goToGames,
  goToPlayers,
  goToFriends,
  goToMessages,
  goToRoot,
}) => (
  <div className={className}>
    <nav className="navbar navbar-expand-lg navbar-light">
      <ul className="navbar-nav navbar-nav-left">
        <NavItem id="games" activeItem={activeItem} onNavigate={goToGames} title="Games" />
        <NavItem id="players" activeItem={activeItem} onNavigate={goToPlayers} title="Players" />
        <NavItem id="friends" activeItem={activeItem} onNavigate={goToFriends} title="Friends" />
        <NavItem id="messages" activeItem={activeItem} onNavigate={goToMessages} title="Messages" />
      </ul>

      <a
        href="/"
        onClick={ev => {
          ev.preventDefault();
          goToRoot();
        }}
        className="navbar-brand"
      >
        <SVGInline svg={playTooLogo} />
        playtoo
      </a>

      <ul className="navbar-nav navbar-nav-right">
        <NavigationDropdown />
      </ul>
    </nav>
  </div>
);

NavBar.propTypes = {
  activeItem: PropTypes.string,
  className: PropTypes.string,
  goToGames: PropTypes.func.isRequired,
  goToPlayers: PropTypes.func.isRequired,
  goToFriends: PropTypes.func.isRequired,
  goToMessages: PropTypes.func.isRequired,
  goToRoot: PropTypes.func.isRequired,
};

NavBar.defaultProps = {
  activeItem: null,
  className: '',
};

export default styled(NavBar)`
  #root & {
    background-color: #272932;
    display: flex;
    justify-content: center;
    box-shadow: 0 3px 0 rgba(0, 0, 0, 0.2);
    position: relative;

    nav {
      width: 1200px;

      height: 64px;
      padding: 0 45px 0 45px;
      align-items: center;

      .navbar-brand,
      .navbar-brand:hover,
      .navbar-brand:focus,
      .navbar-brand:active {
        padding: 0;
        margin: 0;
        margin-top: 5px;
        display: flex;
        color: #fff;
        order: 1;

        svg {
          width: 32px;
          height: 32px;
          display: box-inline;
        }
      }

      .navbar-nav-left {
        order: 0;
        flex-basis: 50%;
        justify-content: left;
        font-family: 'Helvetica';
        text-transform: uppercase;

        .nav-item {
          display: flex;

          &:first-child {
            padding-left: 0px;
          }

          padding-left: 18px;
          padding-right: 18px;
        }

        .nav-item a {
          color: #fff;
          padding: 0px;

          &:hover,
          &:active,
          &:focus {
            color: #fff;
          }
        }

        .nav-item.games.active a {
          color: #f5c929;
        }

        .nav-item.players.active a {
          color: #6b8bcc;
        }

        .nav-item.friends.active a {
          color: #e75753;
        }

        .nav-item.messages.active a {
          color: #6fb8a5;
        }
      }

      .navbar-nav-right {
        order: 2;
        flex-basis: 50%;
        justify-content: flex-end;
      }
    }
  }
`;
