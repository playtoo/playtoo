import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import Navbar from './view';

const SET_PAGE = gql`
  mutation setPage($page: String!) {
    setPage(page: $page) @client {
      page
    }
  }
`;

const GET_PAGE = gql`
  query {
    page @client
  }
`;

export default compose(graphql(GET_PAGE), graphql(SET_PAGE))(
  ({ data: { loading, page }, mutate }) => {
    if (loading) return null;

    return (
      <Navbar
        activeItem={page}
        goToRoot={() => mutate({ variables: { page: '' } })}
        goToGames={() => mutate({ variables: { page: 'games' } })}
        goToPlayers={() => mutate({ variables: { page: 'players' } })}
        goToFriends={() => mutate({ variables: { page: 'friends' } })}
        goToMessages={() => mutate({ variables: { page: 'messages' } })}
      />
    );
  },
);
