import React from 'react';
import { storiesOf } from '../../../lib/storybook';

import readme from './README.md';
import GameCard from './index';

import img from '../../../../.storybook/static/diablo3.jpg';

storiesOf('GameCard', readme, module)
  .add('Default', () => <GameCard name="Game name" image={img} />)
  .add('Long name', () => (
    <GameCard name="A game with a very very long name, part 2" image={img} />
  ));
