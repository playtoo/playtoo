import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const GameCard = ({ className, image, name }) => (
  <div className={className}>
    <img src={image} alt={`Art for ${name}`} />
    <span>{name}</span>
  </div>
);

GameCard.propTypes = {
  className: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default styled(GameCard)`
  display: flex;
  flex-direction: column;
  margin: 50px;
  width: 276px;

  img {
    width: 276px;
    height: 154px;
    border-radius: 5px;
    display: flex;
  }

  span {
    display: flex;
    font-size: 20px;
    font-weight: bold;
  }
}
`;
