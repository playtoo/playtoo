import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const FeaturedGames = ({ className }) => <div className={className} />;
FeaturedGames.propTypes = {
  className: PropTypes.string.isRequired,
};

export default styled(FeaturedGames)`
  #root & {
    display: flex;
    height: 284px;
  }
`;
