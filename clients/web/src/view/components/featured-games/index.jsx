import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import View from './view';
import Loading from './loading';

const GET_FEATURED_GAMES = gql`
  query featuredGames($count: Int!) {
    featuredGames(limit: $count) {
      id
      name
      art
    }
  }
`;

export default compose(
  graphql(GET_FEATURED_GAMES, {
    options: ({ count = 3 }) => ({ variables: { count } }),
  }),
)(({ data: { loading, featuredGames } }) => {
  if (loading) return <Loading />;

  return <View featuredGames={featuredGames} />;
});
