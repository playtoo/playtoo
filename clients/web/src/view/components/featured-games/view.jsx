import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import GameCard from '../game-card';

const FeaturedGames = ({ className, featuredGames }) => (
  <div className={className}>
    {featuredGames.map(game => <GameCard key={game.id} name={game.name} image={game.art} />)}
  </div>
);

FeaturedGames.propTypes = {
  className: PropTypes.string.isRequired,
  featuredGames: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      art: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default styled(FeaturedGames)`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;
