# Edit Linked Accounts

This component is used on the Edit Profile screens, to add more authentication providers to the
user account.

It shows a list of available providers. If the user has already linked a provider with their
account, that povider will appear disabled.

# TODO

* Implement a method to remove providers
* ... but be sure there is always at least one provider selected
* Add confirmation dialog if there is already a different account with another provider asking
  the user to merge them.
