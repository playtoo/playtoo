import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import EditLinkAccounts from './view';

const GET_USER = gql`
  {
    me {
      id
      hasGoogle
      hasFacebook
      hasTwitter
      hasDiscord
    }
  }
`;

const SET_PAGE = gql`
  mutation setPage($page: String!) {
    setPage(page: $page) @client {
      page
    }
  }
`;

export default compose(graphql(GET_USER), graphql(SET_PAGE))(
  ({ data: { loading, me, error }, mutate }) => {
    if (loading) return null;
    if (error) throw error;

    return (
      <EditLinkAccounts
        googleIsLinked={me.hasGoogle}
        facebookIsLinked={me.hasFacebook}
        twitterIsLinked={me.hasTwitter}
        discordIsLinked={me.hasDiscord}
        linkGoogle={() => mutate({ variables: { page: 'google' } })}
        linkFacebook={() => mutate({ variables: { page: 'facebook' } })}
        linkTwitter={() => mutate({ variables: { page: 'twitter' } })}
        linkDiscord={() => mutate({ variables: { page: 'discord' } })}
      />
    );
  },
);
