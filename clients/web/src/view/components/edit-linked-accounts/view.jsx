import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import SVGInline from 'react-svg-inline';

import googleLogo from '../../assets/google.svg';
import twitterLogo from '../../assets/twitter.svg';
import facebookLogo from '../../assets/facebook.svg';
import discordLogo from '../../assets/discord.svg';

const EditLinkAccounts = ({
  className,
  googleIsLinked,
  facebookIsLinked,
  twitterIsLinked,
  discordIsLinked,
  linkGoogle,
  linkFacebook,
  linkTwitter,
  linkDiscord,
}) => (
  <ul className={className}>
    <li>
      <button className="google" disabled={googleIsLinked} onClick={() => linkGoogle()}>
        <SVGInline svg={googleLogo} />
        Google
      </button>
    </li>
    <li>
      <button className="twitter" disabled={twitterIsLinked} onClick={() => linkTwitter()}>
        <SVGInline svg={twitterLogo} />
        Twitter
      </button>
    </li>
    <li>
      <button className="facebook" disabled={facebookIsLinked} onClick={() => linkFacebook()}>
        <SVGInline svg={facebookLogo} />
        Facebook
      </button>
    </li>
    <li>
      <button className="discord" disabled={discordIsLinked} onClick={() => linkDiscord()}>
        <SVGInline svg={discordLogo} />
        Discord
      </button>
    </li>
  </ul>
);

EditLinkAccounts.propTypes = {
  className: PropTypes.string,
  googleIsLinked: PropTypes.bool,
  facebookIsLinked: PropTypes.bool,
  twitterIsLinked: PropTypes.bool,
  discordIsLinked: PropTypes.bool,
  linkGoogle: PropTypes.func.isRequired,
  linkFacebook: PropTypes.func.isRequired,
  linkTwitter: PropTypes.func.isRequired,
  linkDiscord: PropTypes.func.isRequired,
};

EditLinkAccounts.defaultProps = {
  className: '',
  googleIsLinked: false,
  facebookIsLinked: false,
  twitterIsLinked: false,
  discordIsLinked: false,
};

export default styled(EditLinkAccounts)`
  & {
    list-style: none;
    display: flex;

    button {
      display: flex;
      width: 150px;
      height: 64px;
      background-color: white;
      border: 1px solid #ccc;
      border-radius: 3px;
      margin: 50px;
      padding: 0;
      line-height: 64px;

      &:disabled {
        .SVGInline {
          opacity: 0.3;
        }
      }

      .SVGInline {
        width: 48px;
        height: 48px;
        margin: 8px;
        margin-right: 16px;
      }
    }
  }
`;
