import readme from './README.md';
import EditLinkAccounts from './index';
import { storiesOf, withApollo } from '../../../lib/storybook';

storiesOf('EditLinkAccounts', readme, module)
  .add('when loading', () =>
    withApollo({
      Query: {
        me: async () => new Promise(() => {}),
      },
    })(EditLinkAccounts),
  )

  .add('nothing alraedy linked', () =>
    withApollo({
      Query: {
        me: async () => ({
          id: 1,
          hasGoogle: false,
          hasTwitter: false,
          hasFacebook: false,
          hasDiscord: false,
        }),
      },
    })(EditLinkAccounts),
  )

  .add('Google alraedy linked', () =>
    withApollo({
      Query: {
        me: async () => ({
          id: 1,
          hasGoogle: true,
          hasTwitter: false,
          hasFacebook: false,
          hasDiscord: false,
        }),
      },
    })(EditLinkAccounts),
  )

  .add('Twitter alraedy linked', () =>
    withApollo({
      Query: {
        me: async () => ({
          id: 1,
          hasGoogle: false,
          hasTwitter: true,
          hasFacebook: false,
          hasDiscord: false,
        }),
      },
    })(EditLinkAccounts),
  )

  .add('Facebook alraedy linked', () =>
    withApollo({
      Query: {
        me: async () => ({
          id: 1,
          hasGoogle: false,
          hasTwitter: false,
          hasFacebook: true,
          hasDiscord: false,
        }),
      },
    })(EditLinkAccounts),
  )

  .add('Discord alraedy linked', () =>
    withApollo({
      Query: {
        me: async () => ({
          id: 1,
          hasGoogle: false,
          hasTwitter: false,
          hasFacebook: false,
          hasDiscord: true,
        }),
      },
    })(EditLinkAccounts),
  )

  .add('everything alraedy linked', () =>
    withApollo({
      Query: {
        me: async () => ({
          id: 1,
          hasGoogle: true,
          hasTwitter: true,
          hasFacebook: true,
          hasDiscord: true,
        }),
      },
    })(EditLinkAccounts),
  );
