import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import 'react-select/dist/react-select.css';
import classname from 'classnames';

const Tag = ({ label, onDelete, edit }) => (
  <li className={classname({ edit })}>
    {label}
    {edit ? (
      <button type="button" className="delete" aria-label="Delete" onClick={() => onDelete()}>
        <span aria-hidden="true">&times;</span>
      </button>
    ) : null}
  </li>
);

Tag.propTypes = {
  label: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
  edit: PropTypes.bool,
};

Tag.defaultProps = {
  onDelete: null,
  edit: false,
};

const UserTags = ({ className, removeTag, edit, userTags }) => (
  <ul className={className}>
    {userTags.map(({ id, name }) => (
      <Tag edit={edit} key={id} label={name} onDelete={() => removeTag(id)} />
    ))}
  </ul>
);

UserTags.propTypes = {
  removeTag: PropTypes.func.isRequired,
  className: PropTypes.string,
  userTags: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
  edit: PropTypes.bool,
};

UserTags.defaultProps = {
  className: '',
  edit: false,
};

export default styled(UserTags)`
  & {
    display: flex;
    flex-wrap: wrap;
    padding: 0px;

    li {
      display: flex;
      background-color: #6c8bcc;
      color: white;
      margin: 5px;
      padding: 1px 15px;
      border-radius: 12px;
      position: relative;

      button {
        display: none;
        padding: 0px;
        background-color: transparent;
        color: white;
        border: 0px;
        float: right;
        position: absolute;
        left: 0px;
        text-align: center;
        width: 100%;
        cursor: pointer;
      }

      &.edit {
        background-color: #c93833;
      }

      &.edit:hover {
        color: rgba(255, 255, 255, 0.1);

        button {
          display: inline-block;
        }
      }
    }
  }
`;
