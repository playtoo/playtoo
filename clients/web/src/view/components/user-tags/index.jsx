import React from 'react';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import View from './view';

const computeUsedTags = (userTags, allTags) => {
  const allTagsMap = allTags.reduce((acc, { id, name }) => Object.assign(acc, { [id]: name }), {});
  return userTags.map(({ id }) => ({ id, name: allTagsMap[id] }));
};

export const FRAGMENT = gql`
  fragment UserTags on User {
    id
    tags {
      id
    }
  }
`;

const DELETE_TAG = gql`
  mutation removeTagFromUser($tag: String!) {
    removeTagFromUser(tag: $tag) {
      ...UserTags
    }
  }
  ${FRAGMENT}
`;

const GET_TAGS = gql`
  query {
    me {
      ...UserTags
    }
    tags @client {
      id
      name
    }
  }
  ${FRAGMENT}
`;

const EditTags = ({ data: { loading, me, tags }, mutate, edit }) => {
  if (loading) {
    return null;
  }
  return (
    <View
      edit={edit}
      userTags={computeUsedTags(me.tags, tags)}
      removeTag={tag => {
        mutate({ variables: { tag } });
      }}
    />
  );
};

EditTags.propTypes = {
  edit: PropTypes.bool,
  data: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    me: PropTypes.shape({
      tags: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          name: PropTypes.string,
        }),
      ),
    }),
    tags: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string,
      }),
    ),
  }).isRequired,
  mutate: PropTypes.func.isRequired,
};

EditTags.defaultProps = {
  edit: false,
};

export default compose(graphql(GET_TAGS), graphql(DELETE_TAG))(EditTags);
