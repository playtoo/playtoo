// import React from 'react';
// import { storiesOf } from '@storybook/react';
// import { withNotes } from '@storybook/addon-notes';

// import UserTags from './index';
// import readme from './README.md';

// storiesOf('UserTags', module)
//   .decorateWithStore(({ store }) => {
//     store.user.tags.push('chill');
//     store.user.tags.push('support');
//     store.user.tags.push('neutral');
//     store.user.tags.push('mage');
//     store.user.tags.push('achiever');
//   })

//   .addWithStore('default', withNotes(readme)(() => <UserTags />))

//   .addWithStore('in edit mode', () => <UserTags edit />)

//   .addWithStore('user with lots of tags', ({ store }) => {
//     store.user.tags.length = 0;
//     store.user.tags.push('chill');
//     store.user.tags.push('goofy');
//     store.user.tags.push('support');
//     store.user.tags.push('role-player');
//     store.user.tags.push('mage');
//     store.user.tags.push('tank');
//     store.user.tags.push('dps');
//     store.user.tags.push('healer');
//     store.user.tags.push('lawful-good');
//     store.user.tags.push('neutral-good');
//     store.user.tags.push('chaotic-good');
//     store.user.tags.push('lawful-neutral');
//     store.user.tags.push('neutral');
//     store.user.tags.push('chaotic-neutral');
//     store.user.tags.push('lawful-evil');
//     store.user.tags.push('neutral-evil');
//     store.user.tags.push('chaotic-evil');
//     store.user.tags.push('killer');
//     store.user.tags.push('achiever');
//     store.user.tags.push('explorer');
//     store.user.tags.push('socializer');
//     return <UserTags />;
//   });
