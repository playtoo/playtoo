# User Tags

This component displays the list of tags associated with an user.

It can render in view mode (tags are blue) or in edit mode (tags are red and can be deleted).
