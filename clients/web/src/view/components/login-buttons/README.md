# Login Buttons

This component is a dialog that will show up when the user clicks on the 'Login' button.

By default the dialog is closed (i.e. nothing on the screen)

# TODO

* Add a 'Close' button.
* Close it using ESC key too.
