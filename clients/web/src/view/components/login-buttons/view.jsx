import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';
import jquery from 'jquery';

class LoginButtons extends Component {
  constructor() {
    super();
    this.id = uuidv4();
  }

  componentDidMount() {
    if (this.props.open) {
      jquery(`#${this.id}`).modal('show');
    }
    jquery(`#${this.id}`).on('hidden.bs.modal', this.props.closeLogin);
  }

  componentDidUpdate() {
    if (this.props.open) {
      jquery(`#${this.id}`).modal('show');
    }
  }

  componentWillUnmount() {
    jquery(`#${this.id}`).modal('hide');
    jquery(`#${this.id}`).off('hidden.bs.modal', this.props.closeLogin);
  }

  render() {
    const { onLogin } = this.props;
    return (
      <div id={this.id} className="modal" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button type="button" className="btn btn-primary" onClick={() => onLogin('google')}>
                Google
              </button>
              <button type="button" className="btn btn-primary" onClick={() => onLogin('twitter')}>
                Twitter
              </button>
              <button type="button" className="btn btn-primary" onClick={() => onLogin('facebook')}>
                Facebook
              </button>
              <button type="button" className="btn btn-primary" onClick={() => onLogin('discord')}>
                Discord
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LoginButtons.propTypes = {
  onLogin: PropTypes.func.isRequired,
  closeLogin: PropTypes.func.isRequired,
  open: PropTypes.bool,
};

LoginButtons.defaultProps = {
  open: false,
};

export default LoginButtons;
