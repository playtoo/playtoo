import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import LoginButtons from './view';

const GET_SHOW_LOGIN = gql`
  query {
    showLogin @client
  }
`;

const SET_SHOW_LOGIN = gql`
  mutation showLogin($show: Boolean!) {
    showLogin(show: $show) @client
  }
`;

const SET_PAGE = gql`
  mutation setPage($page: String!) {
    setPage(page: $page) @client
  }
`;

export default compose(
  graphql(GET_SHOW_LOGIN),
  graphql(SET_SHOW_LOGIN, { name: 'setShowLogin' }),
  graphql(SET_PAGE, { name: 'setPage' }),
)(({ data: { showLogin }, setShowLogin, setPage }) => (
  <LoginButtons
    open={showLogin}
    onLogin={provider => setPage({ variables: { page: provider } })}
    closeLogin={() => setShowLogin({ variables: { show: false } })}
  />
));

// export default inject(({ store, actions }) => ({
//   onLogin: actions.login,
//   open: store.ui.showLogin,
//   closeLogin: actions.closeLogin,
// }))(LoginButtons);
