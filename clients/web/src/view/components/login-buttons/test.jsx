import React from 'react';
import { shallow } from 'enzyme';
import LoginButtons from './index';

test('Can detect clicks on Google button', () => {
  const fn = jest.fn();

  const buttons = shallow(<LoginButtons onClick={fn} />);
  buttons.find('Button[children="Google"]').simulate('click');

  expect(fn).toBeCalledWith('google');
});
