# Navbar

This component is used to show the user dropdown.

When the user is not authenticated, it will show a Login button. When it is authenticated, it will
show the user avatar and a dropdown menu.
