import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const NavigationDropdown = ({
  className,
  goToMyProfile,
  goToAccount,
  goToLogout,
  goToLogin,
  avatarUrl,
  username,
}) => {
  if (!username) {
    return <button onClick={() => goToLogin()}>Login</button>;
  }

  return (
    <div className={`dropdown ${className}`}>
      <button
        className="btn dropdown-toggle"
        type="button"
        id="dropdownMenuButton"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        <img alt="avatar" src={avatarUrl} className="rounded-circle" />
        {username}
      </button>

      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a
          href="/profile"
          className="dropdown-item"
          onClick={ev => {
            ev.preventDefault();
            goToMyProfile();
          }}
        >
          My Profile
        </a>

        <a
          href="/account"
          className="dropdown-item"
          onClick={ev => {
            ev.preventDefault();
            goToAccount();
          }}
        >
          Account
        </a>
        <a
          href="/logout"
          className="dropdown-item"
          onClick={ev => {
            ev.preventDefault();
            goToLogout();
          }}
        >
          Log Out
        </a>
      </div>
    </div>
  );
};

NavigationDropdown.propTypes = {
  className: PropTypes.string,
  avatarUrl: PropTypes.string,
  username: PropTypes.string,
  goToMyProfile: PropTypes.func.isRequired,
  goToAccount: PropTypes.func.isRequired,
  goToLogout: PropTypes.func.isRequired,
  goToLogin: PropTypes.func.isRequired,
};
NavigationDropdown.defaultProps = {
  className: '',
  avatarUrl: '',
  username: '',
};

export default styled(NavigationDropdown)`
  background-color: #272932;

  .dropdown-toggle {
    margin: 0;
    padding: 0;
    border: 0;
    background-color: transparent;
    color: #fff;
    height: 64px;
    padding-right: 25px;
    padding-left: 25px;
    display: flex;
    justify-content: center;
    min-width: 200px;

    button: {
      width: 100%;
    }

    &::after {
      border: 0;
      content: '⌵';
      vertical-align: inherit;
      margin-left: 15px;
      display: inline-flex;
      height: initial;
    }

    img {
      width: 32px;
      height: 32px;
      margin-right: 13px;
      display: flex;
    }

    &[aria-expanded='true'] {
      background-color: #465e9a;
    }
  }

  .dropdown-menu {
    background-color: #465e9a;
    width: 100%;
    margin: 0;
    padding: 0;
    border: 0;
    border-radius: 0 0 24px 24px;
    box-shadow: 0 3px 0 rgba(0, 0, 0, 0.2);

    a {
      width: calc(100% - 20px);
      color: #fff;
      height: 44px;
      border-top: 1px solid #6b8bcc;
      margin: 0 10px 4px 10px;
      padding: 0px;
      padding-top: 10px;
      text-align: center;

      &:hover {
        color: inherit;
        background-color: inherit;
      }
    }
  }
`;
