import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import NavigationDropdown from './view';

const SET_PAGE = gql`
  mutation setPage($page: String!) {
    setPage(page: $page) @client {
      page
    }
  }
`;

const LOGOUT = gql`
  mutation logout {
    logout @client
  }
`;

const SET_SHOW_LOGIN = gql`
  mutation showLogin($show: Boolean!) {
    showLogin(show: $show) @client
  }
`;

const GET_USER = gql`
  {
    me {
      username
      avatar
      googleAvatar
      twitterAvatar
      facebookAvatar
      discordAvatar
    }
  }
`;

export default compose(
  graphql(GET_USER, { name: 'userData' }),
  graphql(SET_PAGE, { name: 'goTo' }),
  graphql(LOGOUT, { name: 'logout' }),
  graphql(SET_SHOW_LOGIN, { name: 'showLogin' }),
)(({ userData: { loading, me }, logout, goTo, showLogin }) => {
  if (loading) return null;

  const props = {
    goToAccount: () => goTo({ variables: { page: 'account' } }),
    goToMyProfile: () => goTo({ variables: { page: 'edit-profile' } }),
    goToLogin: () => showLogin({ variables: { show: true } }),
    goToLogout: () => logout(),
  };

  if (!me) return <NavigationDropdown {...props} />;

  // Pick the selected provider
  const avatarUrl = (() => {
    switch (me.avatar) {
      case 'google':
        return me.googleAvatar;
      case 'twitter':
        return me.twitterAvatar;
      case 'facebook':
        return me.facebookAvatar;
      case 'discord':
        return me.discordAvatar;
      default:
        return null;
    }
  })();

  return <NavigationDropdown {...props} username={me.username} avatarUrl={avatarUrl} />;
});
