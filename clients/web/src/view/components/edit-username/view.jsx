import React from 'react';
import PropTypes from 'prop-types';
import classname from 'classnames';
import styled from 'styled-components';

import FormField from '../form-field';
import FormButton from '../form-button';

const EditUsername = ({
  className,
  username,
  fieldStatus,
  temporalUsername,
  setUsername,
  checkUsername,
}) => (
  <FormField
    className={className}
    fieldId="username"
    label="Username"
    help="What should people call you?"
    field={
      <input
        type="text"
        data-lpignore="true"
        className={classname('form-control', {
          valid: fieldStatus === 'valid',
          invalid: fieldStatus === 'invalid',
          checking: fieldStatus === 'checking',
          dirty: fieldStatus === 'dirty',
        })}
        onChange={ev => checkUsername(ev.target.value, username)}
        value={temporalUsername || username}
        id="username"
        aria-describedby="usernameHelp"
        placeholder="Enter username"
      />
    }
    secondaryField={
      <>
        {fieldStatus === 'valid' ? (
          <FormButton active onClick={() => setUsername(temporalUsername)}>
            Save
          </FormButton>
        ) : null}
      </>
    }
  />
);

EditUsername.propTypes = {
  className: PropTypes.string,
  username: PropTypes.string,
  fieldStatus: PropTypes.string,
  temporalUsername: PropTypes.string,
  setUsername: PropTypes.func.isRequired,
  checkUsername: PropTypes.func.isRequired,
};

EditUsername.defaultProps = {
  className: '',
  username: '',
  fieldStatus: '',
  temporalUsername: '',
};

export default styled(EditUsername)`
  input {
    height: 50px;
    background-color: #ededed;
    border: 1px solid transparent;
    border-radius: 10px;

    &:focus {
      background-color: #ededed;
    }

    &.dirty,
    &.dirty:focus {
      background-color: #fcf1c9;
    }

    &.checking,
    &.checking:focus {
      background-image: repeating-radial-gradient(circle at center left, #fcf1c9, #fceebf 56px);
      background-size: 200% 100%;
      animation: progress 0.5s linear infinite;
    }

    &.valid,
    &.valid:focus {
      background-color: #daede8;
    }

    &.invalid,
    &.invalid:focus {
      background-color: #f8d4d3;
    }
  }

  @keyframes progress {
    from {
      background-position: -112px;
    }
    to {
      background-position: -56px;
    }
  }
`;
