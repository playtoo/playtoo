import React, { Component } from 'react';
import gql from 'graphql-tag';
import { debounce } from 'lodash';
import PropTypes from 'prop-types';
import { graphql, withApollo } from 'react-apollo';

import View from './view';
import Loading from './loading';
import compose from '../../../lib/compose';

export const FRAGMENT = gql`
  fragment UserUsername on User {
    id
    username
  }
`;

const SET_USERNAME = gql`
  mutation setUsername($username: String!) {
    setUsername(username: $username) {
      ...UserUsername
    }
  }
  ${FRAGMENT}
`;

const GET_USERNAME = gql`
  {
    me {
      ...UserUsername
    }
  }
  ${FRAGMENT}
`;

const VALIDATE_USERNAME = gql`
  query validateUsername($username: String!) {
    user(username: $username) {
      id
    }
  }
`;

class EditUsername extends Component {
  static propTypes = {
    data: PropTypes.shape({
      loading: PropTypes.bool.isRequired,
      me: PropTypes.shape({
        username: PropTypes.string.isRequired,
      }),
    }).isRequired,
    client: PropTypes.shape({
      query: PropTypes.func.isRequired,
    }).isRequired,
    mutate: PropTypes.func.isRequired,
  };

  state = {
    fieldStatus: null,
    temporalUsername: '',
  };

  componentWillUnmount() {
    this.debouncedCheckUsername && this.debouncedCheckUsername.cancel();
  }

  async checkUsername(username) {
    this.setState({ fieldStatus: 'checking' });
    if (username === '') {
      this.setState({ fieldStatus: 'invalid' });
      return;
    }
    const { user } = await this.props.client.query({
      query: VALIDATE_USERNAME,
      variables: { username },
    });
    if (user) this.setState({ fieldStatus: 'invalid' });
    this.setState({ fieldStatus: 'valid' });
  }

  usernameChanged(newUsername, oldUsername) {
    this.debouncedCheckUsername && this.debouncedCheckUsername.cancel();

    if (newUsername === oldUsername) {
      this.setState({ fieldStatus: null, temporalUsername: newUsername });
    } else {
      this.setState({ fieldStatus: 'dirty', temporalUsername: newUsername });
    }

    this.debouncedCheckUsername = debounce(this.checkUsername, 500);
    this.debouncedCheckUsername(newUsername);
  }

  render() {
    const { fieldStatus, temporalUsername } = this.state;
    const { data: { loading, me }, mutate } = this.props;

    if (loading) return <Loading />;

    return (
      <View
        username={me.username}
        fieldStatus={fieldStatus}
        temporalUsername={temporalUsername}
        setUsername={username => {
          mutate({ variables: { username } });
          this.setState({ fieldStatus: null });
        }}
        checkUsername={username => this.usernameChanged(username)}
      />
    );
  }
}

export default compose(withApollo, graphql(GET_USERNAME), graphql(SET_USERNAME))(EditUsername);
