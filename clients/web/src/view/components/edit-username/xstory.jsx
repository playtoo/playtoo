// import React from 'react';
// import { storiesOf } from '@storybook/react';
// import { debounce } from 'lodash';
// import { withNotes } from '@storybook/addon-notes';

// import readme from './README.md';
// import EditUsername from './index';

// storiesOf('EditUsername', module)
//   .decorateWithStore(({ store, actions }) => {
//     store.user.id = '123';
//     store.user.username = 'Batman';

//     const simulateChecking = debounce(() => {
//       store.ui.editUsernameState = 'checking';

//       if (store.ui.temporalUsername === '') {
//         store.ui.editUsernameState = 'invalid';
//         return;
//       }

//       setTimeout(() => {
//         if (store.ui.temporalUsername === 'Superman') store.ui.editUsernameState = 'invalid';
//         else store.ui.editUsernameState = 'valid';
//       }, 3000);
//     }, 500);

//     let timeout;

//     actions.changeUsername = username => {
//       store.ui.editUsernameState = null;
//       simulateChecking.cancel();
//       clearTimeout(timeout);
//       store.ui.temporalUsername = username;
//       store.ui.editUsernameState = 'dirty';
//       simulateChecking();
//     };
//   })

//   .addWithStore('default', withNotes(readme)(() => <EditUsername />))

//   .addWithStore("with changes (won't start checking)", ({ store }) => {
//     store.ui.editUsernameState = 'dirty';
//     return <EditUsername />;
//   })

//   .addWithStore('with valid changes', ({ store }) => {
//     store.ui.editUsernameState = 'valid';
//     return <EditUsername />;
//   })

//   .addWithStore('with invalid changes', ({ store }) => {
//     store.ui.editUsernameState = 'invalid';
//     return <EditUsername />;
//   })

//   .addWithStore("checking if changes are valid (won't stop)", ({ store }) => {
//     store.ui.editUsernameState = 'checking';
//     return <EditUsername />;
//   });
