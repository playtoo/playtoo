import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import FormField from '../form-field';

const EditUsernameLoading = ({ className }) => (
  <FormField
    className={className}
    fieldId="username"
    label="Username"
    help="What should people call you?"
    field={<input type="text" readOnly className="form-control" />}
  />
);

EditUsernameLoading.propTypes = {
  className: PropTypes.string,
};

EditUsernameLoading.defaultProps = {
  className: '',
};

export default styled(EditUsernameLoading)`
  input {
    height: 50px;
    background-color: #ededed;
    border: 1px solid transparent;
    border-radius: 10px;
  }
`;
