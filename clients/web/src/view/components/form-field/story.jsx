import React from 'react';
import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';

import readme from './README.md';
import FormField from './index';

storiesOf('FormField', module)
  .add(
    'Vertical',
    withNotes(readme)(() => (
      <FormField
        label="Main label"
        help="Help text below the label"
        field={<input type="text" id="username" placeholder="Enter username" />}
      />
    )),
  )

  .add(
    'Horizontal',
    withNotes(readme)(() => (
      <FormField
        label="Main label"
        help="Help text below the label"
        secondaryField={<input type="text" id="username" placeholder="Enter username" />}
      />
    )),
  )

  .add(
    'Both',
    withNotes(readme)(() => (
      <FormField
        label="Main label"
        help="Help text below the label"
        field={<input type="text" id="username" placeholder="Enter username" />}
        secondaryField={<input type="text" id="username" placeholder="Enter username" />}
      />
    )),
  );
