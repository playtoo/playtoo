import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classnames from 'classnames';

const FormField = ({ fieldId, className, label, help, field, secondaryField }) => (
  <div
    className={classnames(className, {
      horizontal: !field && secondaryField,
    })}
  >
    <div className="label">
      <label htmlFor={fieldId}>{label}</label>
      <small>{help}</small>
    </div>
    <div className="field">
      <div className="mainField">{field}</div>
      <div className="secondaryField">{secondaryField}</div>
    </div>
  </div>
);

FormField.propTypes = {
  fieldId: PropTypes.string,
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  help: PropTypes.string.isRequired,
  field: PropTypes.node,
  secondaryField: PropTypes.node,
};

FormField.defaultProps = {
  fieldId: null,
  className: null,
  field: null,
  secondaryField: null,
};

export default styled(FormField)`
  #root & {
    display: flex;
    flex-direction: column;
    margin-bottom: 30px;

    .label {
      display: flex;
      flex-direction: column;
      width: 250px;
      flex: 0 auto;
      margin-right: 50px;

      label {
        margin-bottom: 5px;
      }

      small {
        margin-bottom: 10px;
        display: block;
      }
    }

    .field {
      display: flex;
      flex-direction: row;

      .mainField {
        flex: 0 0 auto;
        width: 250px;
        margin-right: 50px;
      }

      .secondaryField {
        width: 500px;
      }
    }

    &.horizontal {
      flex-direction: row;

      .field {
        .mainField {
          display: none;
        }
      }
    }
  }
`;
