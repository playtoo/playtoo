import readme from './README.md';
import EditPublicProfile from './index';
import { storiesOf, withApollo } from '../../../lib/storybook';

const Mutation = {
  setPublicProfile: (obj, { isPublic }) => ({
    id: 1,
    hasPublicProfile: isPublic,
  }),
};

storiesOf('EditPublicProfile', readme, module)
  .add('when loading', () =>
    withApollo({
      Query: {
        me: async () => new Promise(() => {}),
      },
      Mutation,
    })(EditPublicProfile),
  )

  .add('with "Sure" selected', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          hasPublicProfile: true,
        }),
      },
      Mutation,
    })(EditPublicProfile),
  )

  .add('with "No" selected', () =>
    withApollo({
      Query: {
        me: () => ({
          id: 1,
          hasPublicProfile: false,
        }),
      },
      Mutation,
    })(EditPublicProfile),
  );
