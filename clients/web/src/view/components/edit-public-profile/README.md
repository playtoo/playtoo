# Edit Public Play Invites

This component is used on the Edit Profile screens, to select the preference about profile
visibility.

It shows two buttons to enable and disable the setting.

# TODO

* Figure out a way to implement this, is not trivial.
