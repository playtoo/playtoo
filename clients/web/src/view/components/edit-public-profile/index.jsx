import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import compose from '../../../lib/compose';
import View from './view';

export const FRAGMENT = gql`
  fragment UserPublicProfile on User {
    id
    hasPublicProfile
  }
`;

const SET_PUBLIC_PROFILE = gql`
  mutation setPublicProfile($isPublic: Boolean!) {
    setPublicProfile(isPublic: $isPublic) {
      ...UserPublicProfile
    }
  }
  ${FRAGMENT}
`;

const GET_PUBLIC_PROFILE = gql`
  {
    me {
      ...UserPublicProfile
    }
  }
  ${FRAGMENT}
`;

export default compose(graphql(GET_PUBLIC_PROFILE), graphql(SET_PUBLIC_PROFILE))(
  ({ data: { loading, me, error }, mutate }) => {
    if (loading) return <div>Loading...</div>;
    if (error) throw error;

    return (
      <View
        hasPublicProfile={me.hasPublicProfile}
        setPublicProfile={isPublic => mutate({ variables: { isPublic } })}
      />
    );
  },
);
