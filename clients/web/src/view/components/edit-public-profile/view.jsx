import React from 'react';
import PropTypes from 'prop-types';

import FormField from '../form-field';
import FormButton from '../form-button';

const EditPublicProfile = ({ className, setPublicProfile, hasPublicProfile }) => (
  <FormField
    className={className}
    label="Do you want a public profile?"
    help="People can find your profile on Google"
    secondaryField={
      <div className="buttons">
        <FormButton active={hasPublicProfile === true} onClick={() => setPublicProfile(true)}>
          Sure
        </FormButton>
        <FormButton active={hasPublicProfile === false} onClick={() => setPublicProfile(false)}>
          No
        </FormButton>
      </div>
    }
  />
);

EditPublicProfile.propTypes = {
  setPublicProfile: PropTypes.func.isRequired,
  hasPublicProfile: PropTypes.bool,
  className: PropTypes.string,
};

EditPublicProfile.defaultProps = {
  hasPublicProfile: true,
  className: '',
};

export default EditPublicProfile;
