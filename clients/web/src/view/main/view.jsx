import React from 'react';
import PropTypes from 'prop-types';

import NavBar from '../components/navbar';
import LoginButtons from '../components/login-buttons';
import PageEditProfile from '../pages/edit-profile';
import PageGamesList from '../pages/games-list';

const renderPage = page => {
  switch (page) {
    case 'edit-profile':
      return <PageEditProfile />;
    case 'games':
      return <PageGamesList />;
    default:
      return null;
  }
};

const Main = ({ page }) => (
  <div>
    <NavBar />
    <LoginButtons />
    {renderPage(page)}
  </div>
);

Main.propTypes = {
  page: PropTypes.string,
};

Main.defaultProps = {
  page: null,
};
export default Main;
