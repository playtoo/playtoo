import React from 'react';
import gql from 'graphql-tag';
import { compose, graphql } from 'react-apollo';

import Main from './view';

const GET_PAGE = gql`
  query {
    page @client
  }
`;

export default compose(graphql(GET_PAGE))(({ data: { page } }) => <Main page={page} />);
