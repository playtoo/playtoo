import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import EditUsername from '../../components/edit-username';
import EditTags from '../../components/edit-tags';
import EditLinkedAccounts from '../../components/edit-linked-accounts';
import EditEmailNotifications from '../../components/edit-email-notifications';
import EditPublicPlayInvites from '../../components/edit-public-play-invites';
import EditPublicProfile from '../../components/edit-public-profile';
import EditAvatar from '../../components/edit-avatar';
import Title from '../../layout/title';
import Header from '../../layout/header';
import Content from '../../layout/content';
import Section from '../../layout/section';

const PageEditProfile = ({ className }) => (
  <section className={className}>
    <Header edit>
      <Title>Edit Profile</Title>
    </Header>

    <Content>
      <Section title="My Profile">
        <EditAvatar />
        <EditUsername />
        <EditTags />
      </Section>

      <Section title="Settings">
        <EditPublicProfile />
        <EditEmailNotifications />
        <EditPublicPlayInvites />
      </Section>

      <Section title="Linked accounts">
        <EditLinkedAccounts />
      </Section>

      <Section title="Delete account">
        <EditLinkedAccounts />
      </Section>
    </Content>
  </section>
);
PageEditProfile.propTypes = {
  className: PropTypes.string.isRequired,
};

export default styled(PageEditProfile)`
  #root & {
    display: flex;
    flex-direction: column;
  }
`;
