// import React from 'react';
// import { storiesOf } from '@storybook/react';
// import { withNotes } from '@storybook/addon-notes';

// import readme from './README.md';
// import GamesList from './index';

// import diablo3 from '../../../../.storybook/static/diablo3.jpg';
// import forzaHorizon3 from '../../../../.storybook/static/forza-horizon-3.jpg';
// import pubg from '../../../../.storybook/static/pubg.png';
// import seaOfThieves from '../../../../.storybook/static/sea-of-thieves.jpg';

// storiesOf('page/GamesList', module).addWithStore(
//   'default',
//   withNotes(readme)(({ store }) => {
//     store.games.push({
//       id: 'sea-of-thieves',
//       name: 'Sea of Thieves',
//       art: seaOfThieves,
//     });

//     store.games.push({
//       id: 'forza-horizon-3',
//       name: 'Forza Horizon 3',
//       art: forzaHorizon3,
//     });

//     store.games.push({
//       id: 'playerunknowns-battleground',
//       name: "PlayerUnknown's Battleground",
//       art: pubg,
//     });

//     store.games.push({
//       id: 'diablo-3',
//       name: 'Diablo 3',
//       art: diablo3,
//     });

//     return <GamesList />;
//   }),
// );
