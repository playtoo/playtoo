# Page View Profile

This is the View Profile page. This is displayed when the user sees the profile of a different user.

This story **IS NOT** interactive. To see each component working, check the individual story of each
component.
