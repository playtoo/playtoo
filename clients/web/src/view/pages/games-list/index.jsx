import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Title from '../../layout/title';
import Content from '../../layout/content';
import Header from '../../layout/header';
import Section from '../../layout/section';

import FeaturedGames from '../../components/featured-games';
import AllGames from '../../components/all-games';
import SearchBox from '../../components/search-box';

const GamesList = ({ className }) => (
  <section className={className}>
    <Header>
      <Title>Games</Title>
    </Header>

    <Content>
      <Section title="Featured">
        <FeaturedGames />
      </Section>

      <Section title="All games">
        <SearchBox />
        <AllGames />
      </Section>
    </Content>
  </section>
);

GamesList.propTypes = {
  className: PropTypes.string.isRequired,
};

export default styled(GamesList)`
  display: flex;
  flex-direction: column;
`;
