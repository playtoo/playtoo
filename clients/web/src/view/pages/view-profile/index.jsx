import { inject } from 'mobx-react';
import PageViewProfile from './view';

export default inject(({ store }) => ({
  username: store.user.username,
  avatarUrl: store.user.avatarUrl,
  recentGames: store.user.games,
}))(PageViewProfile);
