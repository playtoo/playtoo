import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Moment from 'react-moment';
import Title from '../../layout/title';
import Content from '../../layout/content';
import PageHeader from '../../layout/header';
import UserTags from '../../components/user-tags';

const PageEditProfile = ({ className, avatarUrl, username, recentGames }) => (
  <div className={className}>
    <PageHeader>
      <img alt="avatar" src={avatarUrl} className="rounded-circle avatar" />
      <div className="content">
        <Title>{username}</Title>
        <UserTags />
      </div>
      <div className="buttons">
        <button type="button" className="btn btn-primary">
          Let&apos;s play
        </button>
        <button type="button" className="btn">
          Add friend
        </button>
      </div>
    </PageHeader>
    <Content>
      <section className="played-recently">
        <h2>Played rececently</h2>
        {recentGames.map(game => (
          <div className="game" key={game.id}>
            <img src={game.art} alt={`Art for ${game.name}`} />
            <span className="title">{game.name}</span>
            <Moment interval={0} fromNow>
              {game.lastPlayed}
            </Moment>
          </div>
        ))}
      </section>
      <section className="match-details">
        <h2>Match details</h2>
      </section>
    </Content>
  </div>
);
PageEditProfile.propTypes = {
  className: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  recentGames: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    art: PropTypes.string.isRequired,
    lastPlayed: PropTypes.string.isRequired,
  }).isRequired,
};

export default styled(PageEditProfile)`
  #root & {
    display: flex;
    flex-direction: column;

    ${PageHeader} {
      flex-direction: row;

      img.avatar {
        align-self: center;
        display: flex;
        margin-right: 45px;
        flex: 0 0 auto;
        width: 128px;
        height: 128px;
        box-shadow: rgba(0, 0, 0, 0.2) 0 5px;
      }

      div.content {
        align-self: center;
        display: flex;
        flex: 1 75%;
        flex-direction: column;
      }

      div.buttons {
        align-self: center;
        display: flex;
        flex: 1 25%;
        flex-direction: row;
        margin-left: 100px;
        height: 64px;

        .btn {
          border-radius: 0px;
          margin: 8px;
          height: 45px;
          width: 170px;
          box-shadow: rgba(0, 0, 0, 0.2) 0 3px;
          text-transform: uppercase;
          color: #465e9a;

          &.btn-primary {
            background-color: #6c8bcc;
            border: 0px;
            color: white;

            &:hover {
              background-color: #6c8bcc;
            }
          }
        }
      }
    }

    .match-details {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      flex: 1 50%;
    }

    .played-recently {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      flex: 1 50%;

      h2 {
        flex: 1 100%;
      }
      .game {
        display: flex;
        flex-direction: column;

        img {
          width: 276px;
          height: 154px;
          border-radius: 5px;
          display: flex;
        }

        span.title {
          display: flex;
          font-size: 20px;
          font-weight: bold;
        }

        span.date: {
          display: flex;
          max-width: 250px;
        }
      }
    }
  }
`;
