// import React from 'react';
// import { storiesOf } from '@storybook/react';
// import { withNotes } from '@storybook/addon-notes';

// import readme from './README.md';
// import PageViewProfile from './index';
// import avatar from '../../../../.storybook/static/avatar1.png';

// import diablo3 from '../../../../.storybook/static/diablo3.jpg';
// import forzaHorizon3 from '../../../../.storybook/static/forza-horizon-3.jpg';
// import pubg from '../../../../.storybook/static/pubg.png';
// import seaOfThieves from '../../../../.storybook/static/sea-of-thieves.jpg';

// storiesOf('page/ViewProfile', module).addWithStore(
//   'default',
//   withNotes(readme)(({ store }) => {
//     store.user.avatarProvider = 'google';
//     store.user.googleAvatar = avatar;
//     store.user.username = 'Sergio';

//     store.user.tags = [];
//     store.user.tags.push('chill');
//     store.user.tags.push('goofy');
//     store.user.tags.push('support');
//     store.user.tags.push('role-player');
//     store.user.tags.push('mage');
//     store.user.tags.push('tank');
//     store.user.tags.push('dps');
//     store.user.tags.push('healer');
//     store.user.tags.push('lawful-good');
//     store.user.tags.push('neutral-good');
//     store.user.tags.push('chaotic-good');
//     store.user.tags.push('lawful-neutral');
//     store.user.tags.push('neutral');
//     store.user.tags.push('chaotic-neutral');
//     store.user.tags.push('lawful-evil');
//     store.user.tags.push('neutral-evil');
//     store.user.tags.push('chaotic-evil');
//     store.user.tags.push('killer');
//     store.user.tags.push('achiever');
//     store.user.tags.push('explorer');
//     store.user.tags.push('socializer');

//     store.user.games.push({
//       id: 'sea-of-thieves',
//       name: 'Sea of Thieves',
//       art: seaOfThieves,
//       lastPlayed: new Date(Date.now() - 86400 * 1.5 * 1000),
//     });

//     store.user.games.push({
//       id: 'forza-horizon-3',
//       name: 'Forza Horizon 3',
//       art: forzaHorizon3,
//       lastPlayed: new Date(Date.now() - 86400 * 2.5 * 1000),
//     });

//     store.user.games.push({
//       id: 'playerunknowns-battleground',
//       name: "PlayerUnknown's Battleground",
//       art: pubg,
//       lastPlayed: new Date(Date.now() - 86400 * 7.5 * 1000),
//     });

//     store.user.games.push({
//       id: 'diablo-3',
//       name: 'Diablo 3',
//       art: diablo3,
//       lastPlayed: new Date(Date.now() - 86400 * 15 * 1000),
//     });

//     return <PageViewProfile />;
//   }),
// );
