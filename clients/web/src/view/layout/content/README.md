# Layout Content

This component is main content of the page.

It has a max-width of 1200px and it is centered on the screen.
