import React from 'react';
import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';

import Content from './index';
import readme from './README.md';

storiesOf('layout/Content', module).add('default', withNotes(readme)(() => <Content />));
