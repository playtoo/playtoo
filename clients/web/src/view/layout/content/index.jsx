import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Content = ({ className, children }) => (
  <div className={className}>
    <div>{children}</div>
  </div>
);
Content.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Content.defaultProps = {
  className: '',
};

export default styled(Content)`
  #root & {
    display: flex;
    justify-content: center;

    > div {
      display: flex;
      width: 1200px;
      flex-direction: column;
    }
  }
`;
