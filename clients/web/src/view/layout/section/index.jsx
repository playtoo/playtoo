import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Section = ({ className, children, title, border }) => (
  <div className={classnames(className, { brd: border })}>
    {title ? <h2>{title}</h2> : null}
    <section>{children}</section>
  </div>
);
Section.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
  border: PropTypes.bool,
};

Section.defaultProps = {
  className: '',
  border: true,
};

export default styled(Section)`
  #root & {
    display: flex;
    flex-direction: column;
    padding-top: 20px;
    padding-bottom: 45px;

    /* Bootstrap has its own "border", using a different name to avoid clashes */
    &.brd {
      border-bottom: 1px solid #ccc;
    }

    h2 {
      font-family: 'Tofino-Wide';
      text-transform: uppercase;
      font-size: 20px;
      margin-bottom: 30px;
    }
  }
`;
