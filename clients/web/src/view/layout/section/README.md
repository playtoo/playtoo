# Layout Section

This component represents a section on the page.

Each section has a title and an optional border at the bottom.
