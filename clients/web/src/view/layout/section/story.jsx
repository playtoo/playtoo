import React from 'react';
import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';

import LayouSection from './index';
import readme from './README.md';

storiesOf('layout/Section', module)
  .add(
    'default',
    withNotes(readme)(() => (
      <LayouSection title="Title">
        Cupidatat veniam fugiat veniam duis est esse sint ex laboris aliqua ut. Ut ut fugiat nisi
        elit tempor amet ullamco sunt dolore irure aliquip consectetur. Excepteur consectetur
        exercitation sit ullamco aute ipsum nisi pariatur est dolore nisi laboris consectetur
        adipisicing.
      </LayouSection>
    )),
  )

  .add('without border', () => (
    <LayouSection title="Title" border={false}>
      Cupidatat veniam fugiat veniam duis est esse sint ex laboris aliqua ut. Ut ut fugiat nisi elit
      tempor amet ullamco sunt dolore irure aliquip consectetur. Excepteur consectetur exercitation
      sit ullamco aute ipsum nisi pariatur est dolore nisi laboris consectetur adipisicing.
    </LayouSection>
  ))

  .add('multiple sections together ', () => (
    <>
      <LayouSection title="Title">
        Cupidatat veniam fugiat veniam duis est esse sint ex laboris aliqua ut. Ut ut fugiat nisi
        elit tempor amet ullamco sunt dolore irure aliquip consectetur. Excepteur consectetur
        exercitation sit ullamco aute ipsum nisi pariatur est dolore nisi laboris consectetur
        adipisicing.
      </LayouSection>
      <LayouSection title="Title">
        Non officia elit veniam nostrud ea proident ut exercitation qui. Sunt reprehenderit magna
        laboris enim non. Irure occaecat id sunt pariatur laboris ex deserunt. Officia adipisicing
        labore laborum ex aute cillum officia sunt est ea minim.
      </LayouSection>
      <LayouSection title="Title" border={false}>
        Aliqua aute tempor nisi in cillum ex adipisicing. Nulla aliqua deserunt do fugiat mollit
        laboris id dolore anim dolor fugiat. Mollit non sit elit magna fugiat excepteur. Enim
        proident nostrud Lorem ex non elit in esse. Duis voluptate occaecat consectetur ullamco do
        velit proident ipsum. Ullamco laborum culpa ea aute magna dolore laborum velit exercitation
        dolor anim id. Ipsum minim sit fugiat aute ipsum anim culpa.
      </LayouSection>
    </>
  ));
