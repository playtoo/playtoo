import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import classname from 'classnames';

const Header = ({ className, children, edit }) => (
  <div className={classname(className, { edit })}>
    <div>{children}</div>
  </div>
);
Header.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  edit: PropTypes.bool,
};

Header.defaultProps = {
  className: '',
  edit: false,
};

export default styled(Header)`
  #root & {
    display: flex;
    justify-content: center;
    background-color: #465e9a;
    padding: 45px;
    margin-bottom: 45px;

    &.edit {
      background-color: #c93833;
    }

    > div {
      width: 1200px;
      display: flex;
    }
  }
`;
