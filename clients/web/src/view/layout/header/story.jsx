import React from 'react';
import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';

import Header from './index';
import readme from './README.md';

storiesOf('layout/Header', module)
  .add('default', withNotes(readme)(() => <Header />))
  .add('edit mode', () => <Header edit />)
  .add('view mode', () => <Header />);
