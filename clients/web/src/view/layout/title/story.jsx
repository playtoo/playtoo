import React from 'react';
import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';

import Title from './index';
import Header from '../header';
import readme from './README.md';

storiesOf('layout/Title', module).add(
  'default',
  withNotes(readme)(() => (
    <Header>
      <Title>Title</Title>
    </Header>
  )),
);
