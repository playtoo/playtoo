import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Title = ({ className, children }) => <h1 className={className}>{children}</h1>;

Title.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Title.defaultProps = {
  className: '',
};

export default styled(Title)`
  #root & {
    color: white;
    font-size: 45px;
    font-family: 'Tofino';
    text-shadow: rgba(0, 0, 0, 0.2) 0 6px;
    display: flex;
  }
`;
