import { Router } from 'director';
import { createBrowserHistory } from 'history';
import gql from 'graphql-tag';

const navigate = (url, history) => {
  if (document.location.pathname !== url) {
    history.push(url);
  }
};

const SET_PAGE = gql`
  mutation setPage($page: String!) {
    setPage(page: $page) @client {
      page
    }
  }
`;

const GET_PAGE = gql`
  query {
    page @client
  }
`;

export default class AppRouter {
  constructor({ client }) {
    this.client = client;
    this.history = createBrowserHistory();

    this.router = new Router({
      '/': () => {
        this.client.mutate({ mutation: SET_PAGE, variables: { page: '' } });
      },
      '/friends': () => {
        this.client.mutate({ mutation: SET_PAGE, variables: { page: 'friends' } });
      },
      '/messages': () => {
        this.client.mutate({ mutation: SET_PAGE, variables: { page: 'messages' } });
      },
      '/games': () => {
        this.client.mutate({ mutation: SET_PAGE, variables: { page: 'games' } });
      },
      '/players': () => {
        this.client.mutate({ mutation: SET_PAGE, variables: { page: 'players' } });
      },
      '/edit-profile': () => {
        this.client.mutate({ mutation: SET_PAGE, variables: { page: 'edit-profile' } });
      },
    }).configure({
      run_handler_in_init: true,
      html5history: true,
      notfound: () => {
        this.client.mutate({ mutation: SET_PAGE, variables: { page: '' } });
      },
    });
  }

  init = () => {
    this.router.init();

    this.client
      .watchQuery({
        query: GET_PAGE,
      })
      .subscribe({
        next: ({ data: { page } }) => {
          switch (page) {
            case 'friends':
              navigate('/friends', this.history);
              break;
            case 'messages':
              navigate('/messages', this.history);
              break;
            case 'games':
              navigate('/games', this.history);
              break;
            case 'players':
              navigate('/players', this.history);
              break;
            case 'edit-profile':
              navigate('/edit-profile', this.history);
              break;
            case 'google':
              document.location.assign(`/auth/google`);
              break;
            case 'twitter':
              document.location.assign(`/auth/twitter`);
              break;
            case 'facebook':
              document.location.assign(`/auth/facebook`);
              break;
            case 'discord':
              document.location.assign(`/auth/discord`);
              break;
            case 'logout':
              document.location.assign(`/logout`);
              break;
            default:
              navigate('/', this.history);
          }
        },
      });
  };
}
