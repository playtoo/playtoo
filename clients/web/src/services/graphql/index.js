import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { BatchHttpLink } from 'apollo-link-batch-http';
import { withClientState } from 'apollo-link-state';
import { ApolloLink } from 'apollo-link';

import serverTypeDefs from '../../../../../services/web/src/services/graphql/typeDefs';

export const typeDefs = `
  ${serverTypeDefs}
  extend type Query {
    tags: [Tag]
    page: String
    showLogin: Boolean
    gameSearch: String
  }
  extend type Mutation {
    setPage(page:String!): String
    showLogin(show:Boolean!): Boolean
    setGameSearch(search:String!): String
    logout: Boolean
  }
`;

export const memoryCache = new InMemoryCache();

export const rootValue = {
  page: '',
  showLogin: false,
  gameSearch: '',
  tags: [
    { __typename: 'Tag', id: 'chill', name: 'Chill' },
    { __typename: 'Tag', id: 'goofy', name: 'Goofy' },
    { __typename: 'Tag', id: 'support', name: 'Support' },
    { __typename: 'Tag', id: 'role-player', name: 'Role Player' },
    { __typename: 'Tag', id: 'mage', name: 'Mage' },
    { __typename: 'Tag', id: 'tank', name: 'Tank' },
    { __typename: 'Tag', id: 'dps', name: 'DPS' },
    { __typename: 'Tag', id: 'healer', name: 'Healer' },
    { __typename: 'Tag', id: 'lawful-good', name: 'Lawful Good' },
    { __typename: 'Tag', id: 'neutral-good', name: 'Neutral Good' },
    { __typename: 'Tag', id: 'chaotic-good', name: 'Chaotic Good' },
    { __typename: 'Tag', id: 'lawful-neutral', name: 'Lawful Neutral' },
    { __typename: 'Tag', id: 'neutral', name: 'Neutral' },
    { __typename: 'Tag', id: 'chaotic-neutral', name: 'Chaotic Neutral' },
    { __typename: 'Tag', id: 'lawful-evil', name: 'Lawful Evil' },
    { __typename: 'Tag', id: 'neutral-evil', name: 'Neutral Evil' },
    { __typename: 'Tag', id: 'chaotic-evil', name: 'Chaotic Evil' },
    { __typename: 'Tag', id: 'killer', name: 'Killer' },
    { __typename: 'Tag', id: 'achiever', name: 'Achiver' },
    { __typename: 'Tag', id: 'explorer', name: 'Explorer' },
    { __typename: 'Tag', id: 'socializer', name: 'Socializer' },
  ],
};

export const stateLink = withClientState({
  cache: memoryCache,
  resolvers: {
    Mutation: {
      setPage: (parent, { page }, { cache }) => {
        cache.writeData({ data: { page } });
        return { page };
      },
      showLogin: (parent, { show }, { cache }) => {
        cache.writeData({ data: { showLogin: show } });
        return show;
      },
      logout: (parent, variables, { cache }) => {
        cache.writeData({ data: { me: null } });
        cache.writeData({ data: { page: 'logout' } });
        return false;
      },
      setGameSearch: (parent, { search }, { cache }) => {
        cache.writeData({ data: { gameSearch: search } });
        return search;
      },
    },
  },
  typeDefs,
  defaults: rootValue,
});

export const httpLink = new BatchHttpLink({
  uri: '/graphql',
  credentials: 'same-origin',
});

export const client = new ApolloClient({
  cache: memoryCache,
  link: ApolloLink.from([stateLink, httpLink]),
});
