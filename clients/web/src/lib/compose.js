import { compose } from 'react-apollo';
import { withErrorBoundary } from '../view/components/error-boundary';

export default (...components) => compose(withErrorBoundary, ...components);
