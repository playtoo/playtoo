import React from 'react';
import { storiesOf as baseStoriesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import { ApolloProvider } from 'react-apollo';
import createClient from './apollo-mock';
import ErrorBoundary from '../view/components/error-boundary';
import { typeDefs, rootValue } from '../services/graphql';

const mocks = ({ Query, Mutation = {} }) => ({
  Query: () => Query,
  Mutation: () => Mutation,
});

const withErrorBoundary = getStory => context => <ErrorBoundary>{getStory(context)}</ErrorBoundary>;

export const storiesOf = (name, readme, module) =>
  baseStoriesOf(name, module)
    .addDecorator(story => withNotes(readme)(story)())
    .addDecorator(story => withErrorBoundary(story)());

export const withApollo = query => {
  const client = createClient({
    mocks: mocks(query),
    rootValue,
    apolloLinkOptions: {
      delayMs: 1,
    },
    typeDefs: `
      ${typeDefs}
      directive @client on FIELD
    `,
  });

  return story => <ApolloProvider client={client}>{story()}</ApolloProvider>;
};
