import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '@babel/polyfill';
import { ApolloProvider } from 'react-apollo';

import 'bootstrap';
import 'jquery/dist/jquery.slim';
import 'popper.js';
import './style';

import Main from './view/main';
import Router from './services/router';
import { client } from './services/graphql';

const router = new Router({ client });

class App extends Component {
  render() {
    return (
      <>
        <ApolloProvider client={client}>
          <Main />
        </ApolloProvider>
      </>
    );
  }
}

(async () => {
  // await actions.getOwnUser();
  router.init();
  ReactDOM.render(<App />, document.getElementById('root'));
})();
