import { injectGlobal } from 'styled-components';

// Import external CSS files
import 'bootstrap/dist/css/bootstrap.css';

// Import resources, like fonts, images...
import Tofino from './view/assets/Tofino.otf';
import TofinoWide from './view/assets/Tofino-wide.otf';

// Add these styles to all pages. Keep this to a minimum!
injectGlobal`
  html, body, div#main {
    height: 100%;
  }

  @font-face {
    font-family: 'Tofino';
    src: url(${Tofino});
  }

  @font-face {
    font-family: 'Tofino-Wide';
    src: url(${TofinoWide});
  }
`;
