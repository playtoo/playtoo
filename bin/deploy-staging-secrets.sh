#!/bin/bash

awk 'FNR==1{print ""}{print}' ./infra/env/staging/.* > secrets

docker-cloud service set --env-file secrets psql
docker-cloud service set --env-file secrets web
docker-cloud service set --env-file secrets redis
docker-cloud service set --env-file secrets jobs
docker-cloud stack redeploy staging

rm secrets