# PlayToo

## Status

[![pipeline status](https://gitlab.com/playtoo/playtoo/badges/master/pipeline.svg)](https://gitlab.com/playtoo/playtoo/commits/master)

## Frontend development

### Requisites

1.  Install node https://nodejs.org/en/download/package-manager/#macos
1.  Install yarn https://yarnpkg.com/lang/en/docs/install/

### Using storybook

Run theese commands:

```bash
# Go to the client folder
cd clients/web

# Install all dependencies. This one is only required the first time or after a `git pull`
yarn

# Run Storybook
yarn storybook
```

This will server Storybook on http://localhost:9001/. Open it in a browser and you are redy to go.
After Storybook is started, it will automatically reload any change made to any component.

### Project structure

* `./clients/web/src/view`: Contains all the UI components. Each folder is its own isolated
  componet. Inside each folder, there are the following files:

  * `README.md`: Description of the components. It is shwon on the Storybook UI under the tab named
    'Notes'.
  * `story.jsx`: List of all stories shown on the Storybook for that component. There is a main
    story called `default`, and then stories for each variation of the component. Feel free to add
    more stories to display more states.
  * `view.jsx`: UI of the component. This is where the HTML markup and the CSS styles for the
    component lives. The styles use Styled Components. The syntax is CSS + nesting. See
    https://www.styled-components.com/docs/advanced#existing-css for more info.
  * `index.jsx`: In complex component, this file will contain the adapter to connec the component
    to the state. In simpler components, this file will play the role of `view.jsx`

* `./clients/web/src/style.jsx`: This contains the CSS applied to **all** pages.

* `./clients/web/src/view/assets`: Contains all the assets (images, fonts) used in the UI.
